# Inicializando Ambiente 

* Abra o Docker (se estiver usando seu computador, instale) [Baixe aqui o Docker](https://www.docker.com/)

![alt text](https://i.imgur.com/rPFPMIT.png "AbrindoDocker")

* Clone o Ambiente `git clone https://gitlab.com/gamificacao/ambiente.git`

![alt text](https://i.imgur.com/lYdDC92.png "ClonandoAmbiente")

* Entre no ambiente e digite `./ambienteDev.sh` (se digitar tab dps de ./am ele completa)

![alt text](https://imgur.com/K3DnFfE.png "Entrando no ambiente")
