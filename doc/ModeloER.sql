
/* Drop Tables */

DROP TABLE IF EXISTS public.dialogo;
DROP TABLE IF EXISTS tipo_modal;
DROP TABLE IF EXISTS public.modal;
DROP TABLE IF EXISTS public.idioma;
DROP TABLE IF EXISTS public.log_registro;
DROP TABLE IF EXISTS public.log;
DROP TABLE IF EXISTS public.objeto_em_missao;
DROP TABLE IF EXISTS public.progresso;
DROP TABLE IF EXISTS public.tag_missao;
DROP TABLE IF EXISTS public.missao;
DROP TABLE IF EXISTS public.jogo;
DROP TABLE IF EXISTS public.tag_objeto;
DROP TABLE IF EXISTS public.objeto_jogo;
DROP TABLE IF EXISTS public.tag;
DROP TABLE IF EXISTS public.usuario;



/* Drop Sequences */

DROP SEQUENCE IF EXISTS public.dialogo_id_dialogo_seq;
DROP SEQUENCE IF EXISTS public.idioma_id_idioma_seq;
DROP SEQUENCE IF EXISTS public.jogo_id_jogo_seq;
DROP SEQUENCE IF EXISTS public.log_id_log_seq;
DROP SEQUENCE IF EXISTS public.log_registro_id_log_registro_seq;
DROP SEQUENCE IF EXISTS public.missao_id_missao_seq;
DROP SEQUENCE IF EXISTS public.modal_id_modal_seq;
DROP SEQUENCE IF EXISTS public.objeto_jogo_id_objeto_jogo_seq;
DROP SEQUENCE IF EXISTS public.tag_id_tag_seq;




/* Create Tables */

CREATE TABLE tipo_modal
(
	id_tipo bigserial NOT NULL,
	nome varchar,
	PRIMARY KEY (id_tipo)
) WITHOUT OIDS;


CREATE TABLE public.dialogo
(
	id_dialogo bigserial NOT NULL,
	id_idioma int NOT NULL,
	id_missao int NOT NULL,
	-- No caso de um dialogo ser particionado o auto-relacionamento garante  isso. Um texto longo pode ser autoparticionado dentro do próprio jogo, não sendo necessario usar o auto-relacionamento. 
	id_dialogo_particionado bigint,
	-- Auto-relacionamento para dialogos particionados.
	id_idioma_particionado int,
	dialogo varchar NOT NULL,
	ordem_missao int,
	-- Quando um dialogo é particionado  qual ordem o mesmo deve seguir.
	ordem_particionado int DEFAULT 1 NOT NULL,
	id_tipo bigint NOT NULL,
	CONSTRAINT dialogo_pkey PRIMARY KEY (id_dialogo, id_idioma)
) WITHOUT OIDS;


CREATE TABLE public.idioma
(
	id_idioma serial NOT NULL,
	idioma varchar NOT NULL UNIQUE,
	codigo_i18n varchar,
	CONSTRAINT idioma_pkey PRIMARY KEY (id_idioma)
) WITHOUT OIDS;


CREATE TABLE public.jogo
(
	id_jogo serial NOT NULL,
	nome varchar NOT NULL,
	descricao varchar,
	caminho_url varchar NOT NULL,
	logo oid,
	CONSTRAINT jogo_pkey PRIMARY KEY (id_jogo)
) WITHOUT OIDS;


CREATE TABLE public.log
(
	id_log serial NOT NULL,
	id_missao int NOT NULL,
	id_usuario int NOT NULL,
	hora_inicio timestamp,
	hora_fim timestamp,
	ip varchar,
	user_agent varchar,
	resolucao_tela varchar,
	resolucao_documento varchar,
	CONSTRAINT log_pkey PRIMARY KEY (id_log)
) WITHOUT OIDS;


CREATE TABLE public.log_registro
(
	id_log_registro bigserial NOT NULL,
	id_log int NOT NULL,
	-- Esse Campo poderá ser utilizado caso o objeto esteja armazenado no banco
	id_objeto_origem int,
	-- Esse campo pode ser usado caso o objeto esteja armazenado no banco.
	id_objeto_destino int,
	registro_tempo timestamp,
	acerto boolean,
	registro_info varchar,
	objeto_origem varchar,
	objeto_destino varchar,
	CONSTRAINT log_registro_pkey PRIMARY KEY (id_log_registro)
) WITHOUT OIDS;


CREATE TABLE public.missao
(
	id_missao serial NOT NULL,
	id_jogo int NOT NULL,
	nome varchar,
	descricao varchar,
	CONSTRAINT missao_pkey PRIMARY KEY (id_missao)
) WITHOUT OIDS;


CREATE TABLE public.modal
(
	id_modal bigserial NOT NULL,
	id_idioma int NOT NULL,
	id_missao int NOT NULL,
	nome varchar NOT NULL,
	titulo varchar NOT NULL,
	conteudo varchar NOT NULL,
	texto_aceite varchar,
	texto_rejeite varchar,
	CONSTRAINT modal_pkey PRIMARY KEY (id_modal)
) WITHOUT OIDS;


CREATE TABLE public.objeto_em_missao
(
	id_objeto_jogo int NOT NULL,
	id_missao int NOT NULL,
	css_para_a_missao varchar,
	js_para_a_missao varchar,
	CONSTRAINT objeto_em_missao_pkey PRIMARY KEY (id_objeto_jogo, id_missao)
) WITHOUT OIDS;


CREATE TABLE public.objeto_jogo
(
	id_objeto_jogo serial NOT NULL,
	grafico oid,
	-- O valor poderá ser uma informação que poderá ser usada dentro do jogo.
	valor char,
	-- Código css Adicional que poderá ser aplicado ao elemento para deixar ele de acordo com o ambiente do jogo.
	codigo_css_adicional varchar,
	nome_do_objeto varchar,
	nome_objeto varchar,
	descricao_objeto varchar,
	data_criacao timestamp,
	-- A versão deverá respeitar o padrão 
	-- 
	-- Major.middle.min version
	versao varchar,
	data_alteracao timestamp,
	CONSTRAINT objeto_jogo_pkey PRIMARY KEY (id_objeto_jogo)
) WITHOUT OIDS;


CREATE TABLE public.progresso
(
	id_usuario int NOT NULL,
	id_missao int NOT NULL,
	data_realizado timestamp DEFAULT now() NOT NULL,
	CONSTRAINT progresso_pkey PRIMARY KEY (id_usuario, id_missao, data_realizado)
) WITHOUT OIDS;


CREATE TABLE public.tag
(
	id_tag serial NOT NULL,
	tag varchar,
	CONSTRAINT tag_pkey PRIMARY KEY (id_tag)
) WITHOUT OIDS;


CREATE TABLE public.tag_missao
(
	id_missao int NOT NULL,
	id_tag int NOT NULL,
	CONSTRAINT tag_missao_pkey PRIMARY KEY (id_missao, id_tag)
) WITHOUT OIDS;


CREATE TABLE public.tag_objeto
(
	id_tag int NOT NULL,
	id_objeto_jogo int NOT NULL,
	CONSTRAINT tag_objeto_pkey PRIMARY KEY (id_tag, id_objeto_jogo)
) WITHOUT OIDS;


CREATE TABLE public.usuario
(
	id_usuario int NOT NULL,
	nome_completo varchar NOT NULL,
	matricula varchar NOT NULL,
	foto varchar NOT NULL,
	CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario)
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE public.dialogo
	ADD FOREIGN KEY (id_tipo)
	REFERENCES tipo_modal (id_tipo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.dialogo
	ADD CONSTRAINT dialogo_id_dialogo_particionado_fkey FOREIGN KEY (id_dialogo_particionado, id_idioma_particionado)
	REFERENCES public.dialogo (id_dialogo, id_idioma)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.dialogo
	ADD CONSTRAINT dialogo_id_idioma_fkey FOREIGN KEY (id_idioma)
	REFERENCES public.idioma (id_idioma)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.modal
	ADD CONSTRAINT modal_id_idioma_fkey FOREIGN KEY (id_idioma)
	REFERENCES public.idioma (id_idioma)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.missao
	ADD CONSTRAINT missao_id_jogo_fkey FOREIGN KEY (id_jogo)
	REFERENCES public.jogo (id_jogo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.log_registro
	ADD CONSTRAINT log_registro_id_log_fkey FOREIGN KEY (id_log)
	REFERENCES public.log (id_log)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.dialogo
	ADD CONSTRAINT dialogo_id_missao_fkey FOREIGN KEY (id_missao)
	REFERENCES public.missao (id_missao)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE public.log
	ADD CONSTRAINT log_id_missao_fkey FOREIGN KEY (id_missao)
	REFERENCES public.missao (id_missao)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE public.modal
	ADD CONSTRAINT modal_id_missao_fkey FOREIGN KEY (id_missao)
	REFERENCES public.missao (id_missao)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE public.objeto_em_missao
	ADD CONSTRAINT objeto_em_missao_id_missao_fkey FOREIGN KEY (id_missao)
	REFERENCES public.missao (id_missao)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE public.progresso
	ADD CONSTRAINT progresso_id_missao_fkey FOREIGN KEY (id_missao)
	REFERENCES public.missao (id_missao)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE public.tag_missao
	ADD CONSTRAINT tag_missao_id_missao_fkey FOREIGN KEY (id_missao)
	REFERENCES public.missao (id_missao)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE public.log_registro
	ADD CONSTRAINT log_registro_id_objeto_destino_fkey FOREIGN KEY (id_objeto_destino)
	REFERENCES public.objeto_jogo (id_objeto_jogo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.log_registro
	ADD CONSTRAINT log_registro_id_objeto_origem_fkey FOREIGN KEY (id_objeto_origem)
	REFERENCES public.objeto_jogo (id_objeto_jogo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.objeto_em_missao
	ADD CONSTRAINT objeto_em_missao_id_objeto_jogo_fkey FOREIGN KEY (id_objeto_jogo)
	REFERENCES public.objeto_jogo (id_objeto_jogo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.tag_objeto
	ADD CONSTRAINT tag_objeto_id_objeto_jogo_fkey FOREIGN KEY (id_objeto_jogo)
	REFERENCES public.objeto_jogo (id_objeto_jogo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.tag_missao
	ADD CONSTRAINT tag_missao_id_tag_fkey FOREIGN KEY (id_tag)
	REFERENCES public.tag (id_tag)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.tag_objeto
	ADD CONSTRAINT tag_objeto_id_tag_fkey FOREIGN KEY (id_tag)
	REFERENCES public.tag (id_tag)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.log
	ADD CONSTRAINT log_id_usuario_fkey FOREIGN KEY (id_usuario)
	REFERENCES public.usuario (id_usuario)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public.progresso
	ADD CONSTRAINT progresso_id_usuario_fkey FOREIGN KEY (id_usuario)
	REFERENCES public.usuario (id_usuario)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



/* Comments */

COMMENT ON COLUMN public.dialogo.id_dialogo_particionado IS 'No caso de um dialogo ser particionado o auto-relacionamento garante  isso. Um texto longo pode ser autoparticionado dentro do próprio jogo, não sendo necessario usar o auto-relacionamento. ';
COMMENT ON COLUMN public.dialogo.id_idioma_particionado IS 'Auto-relacionamento para dialogos particionados.';
COMMENT ON COLUMN public.dialogo.ordem_particionado IS 'Quando um dialogo é particionado  qual ordem o mesmo deve seguir.';
COMMENT ON COLUMN public.log_registro.id_objeto_origem IS 'Esse Campo poderá ser utilizado caso o objeto esteja armazenado no banco';
COMMENT ON COLUMN public.log_registro.id_objeto_destino IS 'Esse campo pode ser usado caso o objeto esteja armazenado no banco.';
COMMENT ON COLUMN public.objeto_jogo.valor IS 'O valor poderá ser uma informação que poderá ser usada dentro do jogo.';
COMMENT ON COLUMN public.objeto_jogo.codigo_css_adicional IS 'Código css Adicional que poderá ser aplicado ao elemento para deixar ele de acordo com o ambiente do jogo.';
COMMENT ON COLUMN public.objeto_jogo.versao IS 'A versão deverá respeitar o padrão 

Major.middle.min version';



