<?php

/**
 * Classe controladora referente ao objeto Idioma para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Larissa da Rosa <lariss3012@gmail.com>
 * @version 1.0.0 - 03-08-2017(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class ControladorIdioma extends ControladorAdmin
 {

    /**
     * @var IdiomaDAO
     */
    protected $model;

     /**
      * Construtor da classe Idioma, esse método inicializa o  
      * modelo de dados 
      *
      */
    public function __construct() {
        parent::__construct();
        $this->model = new IdiomaDAO();
    }

     /**
      * Método que redireciona para a página de manter dados  
      *
      */
    public function index()
    {
        $this->manter();
    }

     /**
      * Método que cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Idioma');

        Componente::carregaComponente('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados(BASE_URL . '/admin//idioma/tabela');
        $tabela->setTitulo('Idioma');
        $tabela->addAcaoAdicionar(BASE_URL . 
        '/admin//idioma/criarNovo');
        $tabela->addAcaoEditar(BASE_URL . 
        '/admin//idioma/editar');
        $tabela->addAcaoDeletar(BASE_URL . 
        '/admin//idioma/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id idioma', 'id_idioma');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Idioma', 'idioma');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Código i18n', 'codigo_i18n');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

     /**
      * Método que gera os dados json da tabela de manutenção dos dados 
      * e recebe os dados de consulta para a sua atualizacao 
      *
      */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::carregaComponente('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getTabela($tabela);

        echo json_encode($dados);
    }


     /**
      * Método que controla a inserção de um novo dado
      *
      * @param Idioma $obj - Objeto DataTransfer com os dados da classe
      */
    public function criarNovo(Idioma $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $idioma = $obj == null ? new Idioma() : $obj;

        $this->view->setTitle('Novo Idioma');

        $this->view->attValue('idioma', $idioma);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//idioma/criarNovoFim' . $return);
        $this->view->addTemplate('forms/idioma');
        $this->view->endForm();
    }

    /**
     * Método edita os dados da tabela ou objeto em questão 
     *
     * @param Idioma $obj - Objeto para carregar os formulários 
     */
    public function editar(Idioma $obj = null) 
    {
        if($obj == null){
            $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
            $idioma = $this->model->getById($id);
        }else{
            $idioma = $obj;
        }

        $this->view->setTitle('Editar Idioma');

        $this->view->attValue('idioma', $idioma);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm(BASE_URL . '/admin//idioma/editarFim');
        $this->view->addTemplate('forms/idioma');
        $this->view->endForm();
    }

    /**
     * Método que controla a criação e inserção de um dado no SGBD
     *
     */
    public function criarNovoFim()
     {
        $idioma = new Idioma();
        try {
            unset($_POST['idIdioma']);
            if($idioma->setArrayDados($_POST) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($idioma)){
                 $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                 $this->manter();
                 return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
            }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($idioma);
    }

    /**
     * Método que controla a atualização na tabela 
     *
     */
    public function editarFim()
     {
        $idioma = new Idioma();
        $id = ValidatorUtil::variavelInt($_POST['idIdioma']);
        $idioma->setIdIdioma($id);
        try {
             if($idioma->setArrayDados($_POST) > 0){ 
                 $this->view->addErros($GLOBALS['ERROS']);
             }else{
                 if($this->model->update($idioma)){
                     $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                     $this->manter();
                     return ;
                 }else{
                     $this->view->addMensagemErro($this->model->getErros());
                 }
             }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar($idioma);
    }

    /**
     * Método que controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $idioma = new Idioma();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $idioma->setIdIdioma($id);
        try {
             if($this->model->delete($idioma) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
             }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }

    /**
     * Método que cria os select 
     *
     */
    private function getSelects()
     {
    }

  /**
 * Método que verifica se é um insert em cascade ou seja se é necessário voltar 
  * para um insert em andamento.
  * 
 */
 private function insertCascade()
 {
     if (!empty($this->getArg(0))) {
         $this->redirect('/' . $this->getArg(0) . '/criarNovo');
     }
 }
}
