<?php

/**
 * Classe controladora referente ao objeto Modal para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Larissa da Rosa <lariss3012@gmail.com>
 * @version 1.0.0 - 03-08-2017(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class ControladorModal extends ControladorAdmin
 {

    /**
     * @var ModalDAO
     */
    protected $model;

     /**
      * Construtor da classe Modal, esse método inicializa o  
      * modelo de dados 
      *
      */
    public function __construct() {
        parent::__construct();
        $this->model = new ModalDAO();
    }

     /**
      * Método que redireciona para a página de manter dados  
      *
      */
    public function index()
    {
        $this->manter();
    }

     /**
      * Método que cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Modal');

        Componente::carregaComponente('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados(BASE_URL . '/admin//modal/tabela');
        $tabela->setTitulo('Modal');
        $tabela->addAcaoAdicionar(BASE_URL . 
        '/admin//modal/criarNovo');
        $tabela->addAcaoEditar(BASE_URL . 
        '/admin//modal/editar');
        $tabela->addAcaoDeletar(BASE_URL . 
        '/admin//modal/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id modal', 'id_modal');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('bigint');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Id idioma', 'id_idioma');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Id missão', 'id_missao');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Nome', 'nome');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Título', 'titulo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Conteúdo', 'conteudo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Texto aceite', 'texto_aceite');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Texto rejeite', 'texto_rejeite');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

     /**
      * Método que gera os dados json da tabela de manutenção dos dados 
      * e recebe os dados de consulta para a sua atualizacao 
      *
      */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::carregaComponente('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getTabela($tabela);

        echo json_encode($dados);
    }


     /**
      * Método que controla a inserção de um novo dado
      *
      * @param Modal $obj - Objeto DataTransfer com os dados da classe
      */
    public function criarNovo(Modal $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $modal = $obj == null ? new Modal() : $obj;

        $this->view->setTitle('Novo Modal');

        $this->view->attValue('modal', $modal);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//modal/criarNovoFim' . $return);
        $this->view->addTemplate('forms/modal');
        $this->view->endForm();
    }

    /**
     * Método edita os dados da tabela ou objeto em questão 
     *
     * @param Modal $obj - Objeto para carregar os formulários 
     */
    public function editar(Modal $obj = null) 
    {
        if($obj == null){
            $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
            $modal = $this->model->getById($id);
        }else{
            $modal = $obj;
        }

        $this->view->setTitle('Editar Modal');

        $this->view->attValue('modal', $modal);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm(BASE_URL . '/admin//modal/editarFim');
        $this->view->addTemplate('forms/modal');
        $this->view->endForm();
    }

    /**
     * Método que controla a criação e inserção de um dado no SGBD
     *
     */
    public function criarNovoFim()
     {
        $modal = new Modal();
        try {
            unset($_POST['idModal']);
            if($modal->setArrayDados($_POST) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($modal)){
                 $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                 $this->manter();
                 return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
            }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($modal);
    }

    /**
     * Método que controla a atualização na tabela 
     *
     */
    public function editarFim()
     {
        $modal = new Modal();
        $id = ValidatorUtil::variavelInt($_POST['idModal']);
        $modal->setIdModal($id);
        try {
             if($modal->setArrayDados($_POST) > 0){ 
                 $this->view->addErros($GLOBALS['ERROS']);
             }else{
                 if($this->model->update($modal)){
                     $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                     $this->manter();
                     return ;
                 }else{
                     $this->view->addMensagemErro($this->model->getErros());
                 }
             }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar($modal);
    }

    /**
     * Método que controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $modal = new Modal();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $modal->setIdModal($id);
        try {
             if($this->model->delete($modal) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
             }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }

    /**
     * Método que cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('idioma', 'id_idioma, idioma');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id_idioma', 'idioma');
        $this->view->attValue('listaIdioma', $lista);

        $consulta = $this->model->queryTable('missao', 'id_missao, missao');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id_missao', 'missao');
        $this->view->attValue('listaMissao', $lista);

    }

  /**
 * Método que verifica se é um insert em cascade ou seja se é necessário voltar 
  * para um insert em andamento.
  * 
 */
 private function insertCascade()
 {
     if (!empty($this->getArg(0))) {
         $this->redirect('/' . $this->getArg(0) . '/criarNovo');
     }
 }
}
