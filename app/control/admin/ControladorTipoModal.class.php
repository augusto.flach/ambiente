<?php

/**
 * Classe controladora referente ao objeto Tipo_modal para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Larissa da Rosa <lariss3012@gmail.com>
 * @version 1.0.0 - 03-08-2017(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class ControladorTipoModal extends ControladorAdmin
 {

    /**
     * @var TipoModalDAO
     */
    protected $model;

     /**
      * Construtor da classe Tipo_modal, esse método inicializa o  
      * modelo de dados 
      *
      */
    public function __construct() {
        parent::__construct();
        $this->model = new TipoModalDAO();
    }

     /**
      * Método que redireciona para a página de manter dados  
      *
      */
    public function index()
    {
        $this->manter();
    }

     /**
      * Método que cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Tipo modal');

        Componente::carregaComponente('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados(BASE_URL . '/admin//tipoModal/tabela');
        $tabela->setTitulo('Tipo modal');
        $tabela->addAcaoAdicionar(BASE_URL . 
        '/admin//tipoModal/criarNovo');
        $tabela->addAcaoEditar(BASE_URL . 
        '/admin//tipoModal/editar');
        $tabela->addAcaoDeletar(BASE_URL . 
        '/admin//tipoModal/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id tipo', 'id_tipo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('bigint');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Nome', 'nome');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

     /**
      * Método que gera os dados json da tabela de manutenção dos dados 
      * e recebe os dados de consulta para a sua atualizacao 
      *
      */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::carregaComponente('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getTabela($tabela);

        echo json_encode($dados);
    }


     /**
      * Método que controla a inserção de um novo dado
      *
      * @param TipoModal $obj - Objeto DataTransfer com os dados da classe
      */
    public function criarNovo(TipoModal $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $tipoModal = $obj == null ? new TipoModal() : $obj;

        $this->view->setTitle('Novo Tipo modal');

        $this->view->attValue('tipoModal', $tipoModal);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//tipoModal/criarNovoFim' . $return);
        $this->view->addTemplate('forms/tipo_modal');
        $this->view->endForm();
    }

    /**
     * Método edita os dados da tabela ou objeto em questão 
     *
     * @param TipoModal $obj - Objeto para carregar os formulários 
     */
    public function editar(TipoModal $obj = null) 
    {
        if($obj == null){
            $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
            $tipoModal = $this->model->getById($id);
        }else{
            $tipoModal = $obj;
        }

        $this->view->setTitle('Editar Tipo modal');

        $this->view->attValue('tipoModal', $tipoModal);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm(BASE_URL . '/admin//tipoModal/editarFim');
        $this->view->addTemplate('forms/tipo_modal');
        $this->view->endForm();
    }

    /**
     * Método que controla a criação e inserção de um dado no SGBD
     *
     */
    public function criarNovoFim()
     {
        $tipoModal = new TipoModal();
        try {
            unset($_POST['idTipoModal']);
            if($tipoModal->setArrayDados($_POST) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($tipoModal)){
                 $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                 $this->manter();
                 return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
            }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($tipoModal);
    }

    /**
     * Método que controla a atualização na tabela 
     *
     */
    public function editarFim()
     {
        $tipoModal = new TipoModal();
        $id = ValidatorUtil::variavelInt($_POST['idTipoModal']);
        $tipoModal->setIdTipoModal($id);
        try {
             if($tipoModal->setArrayDados($_POST) > 0){ 
                 $this->view->addErros($GLOBALS['ERROS']);
             }else{
                 if($this->model->update($tipoModal)){
                     $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                     $this->manter();
                     return ;
                 }else{
                     $this->view->addMensagemErro($this->model->getErros());
                 }
             }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar($tipoModal);
    }

    /**
     * Método que controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $tipoModal = new TipoModal();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $tipoModal->setIdTipoModal($id);
        try {
             if($this->model->delete($tipoModal) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
             }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }

    /**
     * Método que cria os select 
     *
     */
    private function getSelects()
     {
    }

  /**
 * Método que verifica se é um insert em cascade ou seja se é necessário voltar 
  * para um insert em andamento.
  * 
 */
 private function insertCascade()
 {
     if (!empty($this->getArg(0))) {
         $this->redirect('/' . $this->getArg(0) . '/criarNovo');
     }
 }
}
