<?php
use core\libs\login\LoginService;
/**
 * Classe principal do sistema responsável por criar a interface padrão assim como verificar 
 * a sessão e permissões do usuário.
 * 
 * @author Marcio Bigolin - <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
abstract class ControladorAdmin extends AbstractController 
{
    private $conteudoMenu;
    protected $modelo;
    #TODO implementar isso em banco
    private $usuariosAdmin = array(
        'marcio.bigolin',
        'sandro.silva',
        '⁠⁠⁠02060092',
        '02150001',
        '02060073',
        '02060143',
        '02060128', // Augusto
        '02150022', // Fabrício
        
    );

    /**
     *
     * @var LoginService
     */
    protected $login;

    public function __construct() 
    {
        $this->modelo = new Modelo();
        $this->view = new VisualizadorAdmin();  
        $this->login = new LoginService(LOGIN_CHAVE, 'usuario');

        if (!$this->verificaLogado()) {
            $this->login->saveRedirect();
            $this->redirect('/login');
        }else{
            $usuario = unserialize($_SESSION['user']);
            if(!in_array($usuario->getLogin(), $this->usuariosAdmin)){
                $this->redirect("/paginaNaoEncontrada");
                exit();
            }
        }
    }  
    
    public function paginaNaoEncontrada() {
        $this->index();
    }
    
    private function verificaLogado()
    {
        return $this->login->verificaLogado();
    }

}
