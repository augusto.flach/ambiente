<?php

/**
 * Classe controladora referente ao objeto Dialogo para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Larissa da Rosa <lariss3012@gmail.com>
 * @version 1.0.0 - 03-08-2017(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class ControladorDialogo extends ControladorAdmin
 {

    /**
     * @var DialogoDAO
     */
    protected $model;

     /**
      * Construtor da classe Dialogo, esse método inicializa o  
      * modelo de dados 
      *
      */
    public function __construct() {
        parent::__construct();
        $this->model = new DialogoDAO();
    }

     /**
      * Método que redireciona para a página de manter dados  
      *
      */
    public function index()
    {
        $this->manter();
    }

     /**
      * Método que cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Diálogo');

        Componente::carregaComponente('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados(BASE_URL . '/admin//dialogo/tabela');
        $tabela->setTitulo('Diálogo');
        $tabela->addAcaoAdicionar(BASE_URL . 
        '/admin//dialogo/criarNovo');
        $tabela->addAcaoEditar(BASE_URL . 
        '/admin//dialogo/editar');
        $tabela->addAcaoDeletar(BASE_URL . 
        '/admin//dialogo/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id diálogo', 'id_dialogo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('bigint');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Id idioma', 'id_idioma');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Id missão', 'id_missao');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Id diálogo particionado', 'id_dialogo_particionado');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('bigint');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Id idioma particionado', 'id_idioma_particionado');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Diálogo', 'dialogo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Ordem missão', 'ordem_missao');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Ordem particionado', 'ordem_particionado');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Id tipo', 'id_tipo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('bigint');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

     /**
      * Método que gera os dados json da tabela de manutenção dos dados 
      * e recebe os dados de consulta para a sua atualizacao 
      *
      */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::carregaComponente('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getTabela($tabela);

        echo json_encode($dados);
    }


     /**
      * Método que controla a inserção de um novo dado
      *
      * @param Dialogo $obj - Objeto DataTransfer com os dados da classe
      */
    public function criarNovo(Dialogo $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $dialogo = $obj == null ? new Dialogo() : $obj;

        $this->view->setTitle('Novo Diálogo');

        $this->view->attValue('dialogo', $dialogo);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//dialogo/criarNovoFim' . $return);
        $this->view->addTemplate('forms/dialogo');
        $this->view->endForm();
    }

    /**
     * Método edita os dados da tabela ou objeto em questão 
     *
     * @param Dialogo $obj - Objeto para carregar os formulários 
     */
    public function editar(Dialogo $obj = null) 
    {
        if($obj == null){
            $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
            $dialogo = $this->model->getById($id);
        }else{
            $dialogo = $obj;
        }

        $this->view->setTitle('Editar Diálogo');

        $this->view->attValue('dialogo', $dialogo);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm(BASE_URL . '/admin/dialogo/editarFim');
        $this->view->addTemplate('forms/dialogo');
        $this->view->endForm();
    }

    /**
     * Método que controla a criação e inserção de um dado no SGBD
     *
     */
    public function criarNovoFim()
     {
        $dialogo = new Dialogo();
        try {
            unset($_POST['idDialogo']);
            if($dialogo->setArrayDados($_POST) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($dialogo)){
                 $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                 $this->manter();
                 return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
            }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($dialogo);
    }

    /**
     * Método que controla a atualização na tabela 
     *
     */
    public function editarFim()
     {
        $dialogo = new Dialogo();
        $id = ValidatorUtil::variavelInt($_POST['idDialogo']);
        $dialogo->setIdDialogo($id);
        try {
             if($dialogo->setArrayDados($_POST) > 0){ 
                 $this->view->addErros($GLOBALS['ERROS']);
             }else{
                 if($this->model->update($dialogo)){
                     $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                     $this->manter();
                     return ;
                 }else{
                     $this->view->addMensagemErro($this->model->getErros());
                 }
             }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar($dialogo);
    }

    /**
     * Método que controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $dialogo = new Dialogo();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $dialogo->setIdDialogo($id);
        try {
             if($this->model->delete($dialogo) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
             }
        }catch (IOErro $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }

    /**
     * Método que cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('idioma', 'id_idioma, idioma');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id_idioma', 'idioma');
        $this->view->attValue('listaIdioma', $lista);

        $consulta = $this->model->queryTable('missao', 'id_missao, missao');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id_missao', 'missao');
        $this->view->attValue('listaMissao', $lista);

        $consulta = $this->model->queryTable('dialogo', 'id_dialogo_particionado, dialogo');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id_dialogo_particionado', 'dialogo');
        $this->view->attValue('listaDialogo', $lista);

        $consulta = $this->model->queryTable('dialogo', 'id_idioma_particionado, dialogo');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id_idioma_particionado', 'dialogo');
        $this->view->attValue('listaDialogo', $lista);

        $consulta = $this->model->queryTable('tipo', 'id_tipo, tipo');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id_tipo', 'tipo');
        $this->view->attValue('listaTipo', $lista);

    }

  /**
 * Método que verifica se é um insert em cascade ou seja se é necessário voltar 
  * para um insert em andamento.
  * 
 */
 private function insertCascade()
 {
     if (!empty($this->getArg(0))) {
         $this->redirect('/' . $this->getArg(0) . '/criarNovo');
     }
 }
}
