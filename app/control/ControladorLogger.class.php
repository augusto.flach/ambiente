<?php

/**
 * Classe principal do sistema responsável por criar a interface padrão assim como verificar 
 * a sessão e permissões do usuário.
 * 
 * @author Marcio Bigolin - <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class ControladorLogger extends ControladorGeral {

    public function __construct() {
        parent::__construct();
        $this->view->setRenderizado();
    }

    public function getIdLog() {
        echo $this->_getIdLog();
    }

    public function iniciaLog() {
        $dao = new LogDAO();
        $dao->DB()->debugOn();
        $log = new Log();
        $this->verificaUsuario();
        ds($_POST);
        $log->setArrayDados($_POST);
        $log->setIdUsuario($this->getIdUsuario());
        $log->setIp($_SERVER['REMOTE_ADDR']);
        $log->setUserAgent($_SERVER['HTTP_USER_AGENT']);
        ds($log);
        $idLog = $dao->inserirEmTransacao($log);
        $this->add('idLog', $idLog);
        echo $idLog;
    }

    public function gravaLog() {
        $logRegistro = new LogRegistro();
        $dao = new LogRegistroDAO();
        $dao->DB()->debugOn();
        $logRegistro->setArrayDados($_POST);
        $logRegistro->setIdLog($this->get('idLog'));
        ds($_POST);
        $dao->save($logRegistro);
    }

    public function finalizaLog() {
        $dao = new LogDAO();
        //$dao->DB()->debugOn();
        $id = $_POST['idLog'];
        $log = $dao->getByID($id);
        $log->setHoraFim();
    }

    private function getIdUsuario() {

        if ($_SESSION['user']) {
            $usuario = unserialize($_SESSION['user']);
            return $usuario->getId();
        }
    }

    private function _getIdLog() {
        $id = $this->get('idLog');
        if ($id !== null) {
            return $id;
        } else {
            return -1;
        }
    }

    private function verificaUsuario() {
        $dao = new UsuarioDAO();
        $dao->DB()->debugOn();
        $usuario = $dao->getByID($this->getIdUsuario());
        if ($usuario) {
            return true;
        } else {
            $usuarioSessao = unserialize($_SESSION['user']);
            $usuario = new Usuario();
            ds($usuarioSessao);
            $usuario->setIdUsuario($usuarioSessao->getId());
            $usuario->setNomeCompleto($usuarioSessao->getExtra("nome"));
            $usuario->setMatricula($usuarioSessao->getLogin());
            $usuario->setFoto($usuarioSessao->getExtra("foto"));
            $dao->saveId($usuario);
        }
    }

//    public function register(){
//        $dao = new LogDAO();
//        $log = new Log();
//        $log->setArrayDados($_POST);
//        $dao->save($log);
//    }
}
