<?php


/**
 * Classe principal do sistema responsável por criar a interface padrão assim como verificar
 * a sessão e permissões do usuário.
 *
 * @author Marcio Bigolin - <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class ControladorGameService extends ControladorGeral
{

    public function __construct()
    {
        parent::__construct();
        $this->view->setRenderizado();
    }

    public function requireIdUser(){
        echo $this->getIdUsuario();
    }


    public function requireCompleteUser(){ // TODO verificar se está sendo usado
        if($_SESSION['user']){
            $usuario = unserialize($_SESSION['user']);
            echo json_encode($usuario);
        }
    }

    public function requireUserEscape(){
        if($_SESSION['user']){
            $usuario = unserialize($_SESSION['user']);
            $user = new Usuario();
            $user->setNomeCompleto($usuario->getExtra('nome'));
            $user->setFoto($usuario->getExtra('foto'));
            echo json_encode($user);
        }
    }

    public function saveProgress(){
        if($_SESSION['user']){
            $data['id_missao'] = $_POST['id_missao'];
            $data['id_usuario'] = unserialize($_SESSION['user'])->getId();
            $data['xp'] = $_POST['xp'];
            $user = new UsuarioDAO();
            $user->saveProgress($data);
            echo '1';
        }
    }

    public function getLevel(){
        if($_SESSION['user']){
            $data['id_usuario'] = unserialize($_SESSION['user'])->getId();
            $user = new UsuarioDAO();
            $resp = $user->DB()->query('SELECT max(id_missao) FROM progresso WHERE id_usuario = ' . $data['id_usuario']);
            $resp = $user->resultAssoc($resp);
            //if($resp == '') $resp = 1;


            echo $resp[0];
            return;
        }
    }

    public function getXp(){
        if($_SESSION['user']){
            $data['id_usuario'] = unserialize($_SESSION['user'])->getId();
            $user = new UsuarioDAO();
            $sql = 'select sum(exp) from (select max(xp) as exp from progresso where id_usuario = '.$data['id_usuario'].' group by id_missao) experiencia';
            $resp = $user->DB()->query($sql);
            $resp = $user->resultAssoc($resp);
            //if($resp == '') $resp = 0;

            echo $resp[0];
            return;
        }
    }



    public function iniciaLog(){
        if($_SESSION['user']){

          $log = new LogDAO();
          $usuario = unserialize($_SESSION['user'])->getId();
          $idMissao = $_POST['id_missao'];
          $hora = $_POST['hora_inicio'];

          $sql = "INSERT INTO log (id_missao, id_usuario, hora_inicio) VALUES ($idMissao, $usuario, '$hora')";

          echo $log->inserirEmTransacaoSQL($sql);

        }
    }




    public function registro(){
        if($_SESSION['user']){

          $log = new LogRegistroDAO();
          $idLog = $_POST['id_log'];
          $hora = $_POST['registro_horario'];
          $acerto = $_POST['acerto'];
          $desc = $_POST['registro_info'];

          $sql = "INSERT INTO log_registro (id_log, registro_horario, acerto, registro_info) VALUES ($idLog, '$hora', '$acerto', '$desc')";

          echo $log->DB()->exec($sql);

        }
    }


    public function finalizaLog(){
        if($_SESSION['user']){

          $log = new LogDAO();
          $idLog = $_POST['id_log'];
          $hora = $_POST['hora_fim'];

          $sql = "update log set hora_fim = '$hora' where id_log = $idLog";

          echo $log->DB()->exec($sql);

        }
    }



    public function getDialogos(){
        $dao = new DialogoDAO();
        echo json_encode($dao->getArvore());
    }

    public function getStatus(){

    }

    public function getQuestao(){

    }

    public function viewFoto(){
        $this->view->setRenderizado();
        $user = unserialize($_SESSION['user']);
        header('Content-type: image/jpeg');
        echo file_get_contents($user->getExtra('foto'));
    }

    private function getIdUsuario(){
        if($_SESSION['user']){
            $usuario = unserialize($_SESSION['user']);
            return $usuario->getId();
        }
    }



}
