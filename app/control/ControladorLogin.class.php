<?php

use core\libs\login\LoginMoodle;

/**
 * Classe principal do sistema responsável por criar a interface padrão assim como verificar
 * a sessão e permissões do usuário.
 *
 * @author Marcio Bigolin - <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class ControladorLogin extends AbstractController
{

    public function __construct()
    {
        $this->modelo = new Modelo();
        $this->view = new VisualizadorLogin();
    }

    public function paginaNaoEncontrada()
    {
        $this->view->setTitle('Funcionalidade ainda não implementada');
    }

    public function index()
    {
        $this->view->setTitle('Logar no sistema');
        $this->view->attValue('url', $_SERVER['HTTP_HOST']);
        $this->view->addTemplate('nao_logado');
    }

    public function logar()
    {
        $usuario = filter_input(INPUT_POST, 'usuario');
        $senha = filter_input(INPUT_POST, 'senha');

        $url = 'https://moodle.canoas.ifrs.edu.br/login/token.php';
        $login = new LoginMoodle(LOGIN_CHAVE, $url);

        $result = $login->verificaLoginSenha($usuario, $senha, true);
        if ($result) {

            $this->verificaUsuario();

            $login->redirect($this, '/');
            exit();
        } else {
            $this->view->addErro('Login e Senha incorreto');
        }

        $this->index();
    }
    

    private function getIdUsuario() {

        if ($_SESSION['user']) {
            $usuario = unserialize($_SESSION['user']);
            return $usuario->getId();
        }
    }
    
    
    private function verificaUsuario() {
        $dao = new UsuarioDAO();
        $dao->DB()->debugOn();
        $usuario = $dao->getByID($this->getIdUsuario());
        if ($usuario) {
            return true;
        } else {
            $usuarioSessao = unserialize($_SESSION['user']);
            $usuario = new Usuario();
            ds($usuarioSessao);
            $usuario->setIdUsuario($usuarioSessao->getId());
            $usuario->setNomeCompleto($usuarioSessao->getExtra("nome"));
            $usuario->setMatricula($usuarioSessao->getLogin());
            $usuario->setFoto($usuarioSessao->getExtra("foto"));
            $dao->saveId($usuario);
        }
    }
    
    
    
    
    
    

    public function logout()
    {
        unset($_SESSION['user']);
        session_destroy();
        $this->redirect('/');
    }

}
