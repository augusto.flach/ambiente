<?php

/**
 * Classe de modelo referente ao objeto Dialogo para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 08-08-2017(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class DialogoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe DialogoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Diálogo
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.dialogo', 'count(id_dialogo) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.dialogo ' . $tabela->getcondicao(), 
                                         'id_dialogo as principal ,
                                          id_idioma,
                                          id_missao,
                                          id_dialogo_particionado,
                                          id_idioma_particionado,
                                          dialogo,
                                          ordem_missao,
                                          ordem_particionado,
                                          id_tipo'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $dialogo = $this->setDados($linhaBanco);
            $row['id'] = $dialogo->getidDialogo();
            $row['cell'] = $dialogo->getArrayJson();
            $dados[] = $row;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

     /**
     * Método que retorna um objeto do tipo Dialogo
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return Dialogo - Objeto data transfer
     */
    public function getByID($id) {
        $dialogo = new Dialogo();
        $consulta = $this->queryTable($dialogo->getTable(),
                                         'id_dialogo as principal ,
                                          id_idioma,
                                          id_missao,
                                          id_dialogo_particionado,
                                          id_idioma_particionado,
                                          dialogo,
                                          ordem_missao,
                                          ordem_particionado,
                                          id_tipo',
                        'id_dialogo ='. $id );
        if ($consulta) {
            $dialogo = $this->setDados($consulta->fetch());
            return $dialogo;
        } else {
             throw new EntradaDeDadosException();
        }
     }
     /**
     * Método que retorna um array de objetos Dialogo
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos Dialogo
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.dialogo ', 
                                         'id_dialogo as principal ,
                                          id_idioma,
                                          id_missao,
                                          id_dialogo_particionado,
                                          id_idioma_particionado,
                                          dialogo,
                                          ordem_missao,
                                          ordem_particionado,
                                          id_tipo',
            $condicao);
        foreach ($result as $linhaBanco) {
            $dialogo = $this->setDados($linhaBanco);
            $dados[] = $dialogo;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado Dialogo
     * com objetivo de servir as funções getTabela, getLista e getDialogo
     *
     * @param array $linha
     * @return objeto Dialogo
     */
    private function setDados($dados)
    {
        $dialogo = new Dialogo();
        $dialogo->setIdDialogo($dados['principal']);
        $dialogo->setIdIdioma($dados['id_idioma']);
        $dialogo->setIdMissao($dados['id_missao']);
        $dialogo->setIdDialogoParticionado($dados['id_dialogo_particionado']);
        $dialogo->setIdIdiomaParticionado($dados['id_idioma_particionado']);
        $dialogo->setDialogo($dados['dialogo']);
        $dialogo->setOrdemMissao($dados['ordem_missao']);
        $dialogo->setOrdemParticionado($dados['ordem_particionado']);
        $dialogo->setIdTipo($dados['id_tipo']);
        return $dialogo;
    }

    /**
     * Método que insere um objeto do tipo Dialogo
     * na tabela do banco de dados
     *
     * @param Dialogo Objeto data transfer
     */
    public function inserirEmTransacao(Dialogo $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'public.dialogo_id_dialogo_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
}