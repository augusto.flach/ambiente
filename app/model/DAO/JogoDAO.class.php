<?php

/**
 * Classe de modelo referente ao objeto Jogo para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 08-08-2017(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class JogoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe JogoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Jogo
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.jogo', 'count(id_jogo) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.jogo ' . $tabela->getcondicao(), 
                                         'id_jogo as principal ,
                                          nome,
                                          descricao,
                                          caminho_url,
                                          logo'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $jogo = $this->setDados($linhaBanco);
            $row['id'] = $jogo->getidJogo();
            $row['cell'] = $jogo->getArrayJson();
            $dados[] = $row;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

     /**
     * Método que retorna um objeto do tipo Jogo
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return Jogo - Objeto data transfer
     */
    public function getByID($id) {
        $jogo = new Jogo();
        $consulta = $this->queryTable($jogo->getTable(),
                                         'id_jogo as principal ,
                                          nome,
                                          descricao,
                                          caminho_url,
                                          logo',
                        'id_jogo ='. $id );
        if ($consulta) {
            $jogo = $this->setDados($consulta->fetch());
            return $jogo;
        } else {
             throw new EntradaDeDadosException();
        }
     }
     /**
     * Método que retorna um array de objetos Jogo
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos Jogo
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.jogo ', 
                                         'id_jogo as principal ,
                                          nome,
                                          descricao,
                                          caminho_url,
                                          logo',
            $condicao);
        foreach ($result as $linhaBanco) {
            $jogo = $this->setDados($linhaBanco);
            $dados[] = $jogo;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado Jogo
     * com objetivo de servir as funções getTabela, getLista e getJogo
     *
     * @param array $linha
     * @return objeto Jogo
     */
    private function setDados($dados)
    {
        $jogo = new Jogo();
        $jogo->setIdJogo($dados['principal']);
        $jogo->setNome($dados['nome']);
        $jogo->setDescricao($dados['descricao']);
        $jogo->setCaminhoUrl($dados['caminho_url']);
        $jogo->setLogo($dados['logo']);
        return $jogo;
    }

    /**
     * Método que insere um objeto do tipo Jogo
     * na tabela do banco de dados
     *
     * @param Jogo Objeto data transfer
     */
    public function inserirEmTransacao(Jogo $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'public.jogo_id_jogo_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
}