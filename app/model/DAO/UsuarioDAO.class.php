<?php

/**
 * Classe de modelo referente ao objeto Usuario para
 * a manutenção dos dados no sistema
 *
 * @package modulos.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 25-02-2016(Gerado automaticamente - GC - 1.0 02/11/2015)
 */

class UsuarioDAO extends AbstractDAO
{

    /**
    * Construtor da classe UsuarioDAO esse metodo
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Usuário
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.usuario', 'count(id_usuario) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.usuario ' . $tabela->getcondicao(),
                                         'id_usuario as principal ,
                                          nome_completo,
                                          matricula,
                                          usuario'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $linha = array();
            $usuario = $this->setDados($linhaBanco);
            $linha['id'] = $usuario->getidUsuario();
            $linha['cell'] = $usuario->getArrayJson();
            $dados[] = $linha;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

    public function saveId(Usuario $usuario){
      $nome =  $usuario->getNomeCompleto();
      $idUsuario = $usuario->getIdUsuario();
      $matricula = $usuario->getMatricula();
      $foto = $usuario->getFoto();

      $sql = "INSERT INTO usuario (nome_completo,id_usuario,matricula,foto) VALUES";
      $sql .= "('$nome','$idUsuario','$matricula','$foto')";

      $this->db->exec($sql);

    }


    public function saveProgress($data){

      $idMissao = $data['id_missao'];
      $id = $data['id_usuario'];
      $xp = $data['xp'];
      $sql = "INSERT INTO progresso (id_usuario, id_missao, xp) VALUES ";
      $sql .= "('$id', '$idMissao', '$xp')";



      $this->db->exec($sql);

    }




    /**
     * Método que retorna um objeto do tipo Usuario
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return Usuario - Objeto data transfer
     */
    public function getByID($id) {
        $usuario = new Usuario();
        $consulta = $this->queryTable($usuario->getTable(),
                                         'id_usuario as principal ,
                                          nome_completo,
                                          matricula,
                                          usuario',
                        'id_usuario ='. $id );
        if ($consulta) {
            $dados = $consulta->fetch();
            if($dados){
                $usuario = $this->setDados($dados);
                return $usuario;
            }
        } else {
             throw new EntradaDeDadosException();
        }
        return false;
     }
     /**
     * Método que retorna um array de objetos Usuario
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos Usuario
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.usuario ',
                                         'id_usuario as principal ,
                                          nome_completo,
                                          matricula,
                                          usuario',
            $condicao);
        foreach ($result as $linhaBanco) {
            $usuario = $this->setDados($linhaBanco);
            $dados[] = $usuario;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado Usuario
     * com objetivo de servir as funções getTabela, getLista e getUsuario
     *
     * @param array $linha
     * @return objeto Usuario
     */
    private function setDados($dados)
    {
        $usuario = new Usuario();
        $usuario->setIdUsuario($dados['principal']);
        $usuario->setNomeCompleto($dados['nome_completo']);
        $usuario->setMatricula($dados['matricula']);
        $usuario->setUsuario($dados['usuario']);
        return $usuario;
    }

    /**
     * Método que insere um objeto do tipo Usuario
     * na tabela do banco de dados
     *
     * @param Usuario Objeto data transfer
     */
    public function inserirEmTransacao(Usuario $obj)
    {
        $this->begin();
        if ($this->db->insert($obj->getTable(), $obj->getArrayDados())) {
            $sequencia = 'public.usuario_id_usuario_seq';
        $id = $this->conexao->getLink()->lastInsertId($sequencia);
        $this->db->commit();
        return $id;
    } else {
        return false;
    }
   }
}
