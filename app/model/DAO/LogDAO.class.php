<?php

/**
 * Classe de modelo referente ao objeto Log para
 * a manutenção dos dados no sistema
 *
 * @package modulos.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 31-08-2017(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class LogDAO extends AbstractDAO
{

    /**
    * Construtor da classe LogDAO esse metodo
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Log
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.log', 'count(id_log) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.log ' . $tabela->getcondicao(),
                                         'id_log as principal ,
                                          id_missao,
                                          id_usuario,
                                          hora_inicio,
                                          hora_fim,
                                          ip,
                                          user_agent,
                                          resolucao_tela,
                                          resolucao_documento'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $log = $this->setDados($linhaBanco);
            $row['id'] = $log->getidLog();
            $row['cell'] = $log->getArrayJson();
            $dados[] = $row;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

     /**
     * Método que retorna um objeto do tipo Log
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return Log - Objeto data transfer
     */
    public function getByID($id) {
        $log = new Log();
        $consulta = $this->queryTable($log->getTable(),
                                         'id_log as principal ,
                                          id_missao,
                                          id_usuario,
                                          hora_inicio,
                                          hora_fim,
                                          ip,
                                          user_agent,
                                          resolucao_tela,
                                          resolucao_documento',
                        'id_log ='. $id );
        if ($consulta) {
            $log = $this->setDados($consulta->fetch());
            return $log;
        } else {
             throw new EntradaDeDadosException();
        }
     }
     /**
     * Método que retorna um array de objetos Log
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos Log
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.log ',
                                         'id_log as principal ,
                                          id_missao,
                                          id_usuario,
                                          hora_inicio,
                                          hora_fim,
                                          ip,
                                          user_agent,
                                          resolucao_tela,
                                          resolucao_documento',
            $condicao);
        foreach ($result as $linhaBanco) {
            $log = $this->setDados($linhaBanco);
            $dados[] = $log;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado Log
     * com objetivo de servir as funções getTabela, getLista e getLog
     *
     * @param array $linha
     * @return objeto Log
     */
    private function setDados($dados)
    {
        $log = new Log();
        $log->setIdLog($dados['principal']);
        $log->setIdMissao($dados['id_missao']);
        $log->setIdUsuario($dados['id_usuario']);
        $log->setHoraInicio($dados['hora_inicio']);
        $log->setHoraFim($dados['hora_fim']);
        $log->setIp($dados['ip']);
        $log->setUserAgent($dados['user_agent']);
        $log->setResolucaoTela($dados['resolucao_tela']);
        $log->setResolucaoDocumento($dados['resolucao_documento']);
        return $log;
    }

    /**
     * Método que insere um objeto do tipo Log
     * na tabela do banco de dados
     *
     * @param Log Objeto data transfer
     */
    public function inserirEmTransacao(Log $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'public.log_id_log_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
    public function inserirEmTransacaoSQL($sql)
    {
        $this->DB()->begin();
        if ($this->db->exec($sql)) {
            $sequencia = 'public.log_id_log_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
}
