<?php

/**
 * Classe de modelo referente ao objeto Missao para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.gamification
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 27-06-2017(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class MissaoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe MissaoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Missão
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.missao', 'count(id_missao) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.missao ' . $tabela->getcondicao(), 
                                         'id_missao as principal ,
                                          id_jogo,
                                          nome_missao,
                                          descricao_missao'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $missao = $this->setDados($linhaBanco);
            $row['id'] = $missao->getidMissao();
            $row['cell'] = $missao->getArrayJson();
            $dados[] = $row;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

     /**
     * Método que retorna um objeto do tipo Missao
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return Missao - Objeto data transfer
     */
    public function getByID($id) {
        $missao = new Missao();
        $consulta = $this->queryTable($missao->getTable(),
                                         'id_missao as principal ,
                                          id_jogo,
                                          nome_missao,
                                          descricao_missao',
                        'id_missao ='. $id );
        if ($consulta) {
            $missao = $this->setDados($consulta->fetch());
            return $missao;
        } else {
             throw new EntradaDeDadosException();
        }
     }
     /**
     * Método que retorna um array de objetos Missao
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos Missao
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.missao ', 
                                         'id_missao as principal ,
                                          id_jogo,
                                          nome_missao,
                                          descricao_missao',
            $condicao);
        foreach ($result as $linhaBanco) {
            $missao = $this->setDados($linhaBanco);
            $dados[] = $missao;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado Missao
     * com objetivo de servir as funções getTabela, getLista e getMissao
     *
     * @param array $linha
     * @return objeto Missao
     */
    private function setDados($dados)
    {
        $missao = new Missao();
        $missao->setIdMissao($dados['principal']);
        $missao->setIdJogo($dados['id_jogo']);
        $missao->setNomeMissao($dados['nome_missao']);
        $missao->setDescricaoMissao($dados['descricao_missao']);
        return $missao;
    }

    /**
     * Método que insere um objeto do tipo Missao
     * na tabela do banco de dados
     *
     * @param Missao Objeto data transfer
     */
    public function inserirEmTransacao(Missao $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'public.missao_id_missao_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
}