<?php

/**
 * Classe de modelo referente ao objeto Elemento para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.gamification
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 29-07-2016(Gerado automaticamente - GC - 1.0 02/11/2015)
 */

class ElementoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe ElementoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Elemento
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.elemento', 'count(id_elemento) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.elemento ' . $tabela->getcondicao(), 
                                         'id_elemento as principal ,
                                          rotulo_elemento,
                                          identificador_codigo,
                                          descricao'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $elemento = $this->setDados($linhaBanco);
            $row['id'] = $elemento->getidElemento();
            $row['cell'] = $elemento->getArrayJson();
            $dados[] = $row;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

     /**
     * Método que retorna um objeto do tipo Elemento
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return Elemento - Objeto data transfer
     */
    public function getByID($id) {
        $elemento = new Elemento();
        $consulta = $this->queryTable($elemento->getTable(),
                                         'id_elemento as principal ,
                                          rotulo_elemento,
                                          identificador_codigo,
                                          descricao',
                        'id_elemento ='. $id );
        if ($consulta) {
            $elemento = $this->setDados($consulta->fetch());
            return $elemento;
        } else {
             throw new EntradaDeDadosException();
        }
     }
     /**
     * Método que retorna um array de objetos Elemento
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos Elemento
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.elemento ', 
                                         'id_elemento as principal ,
                                          rotulo_elemento,
                                          identificador_codigo,
                                          descricao',
            $condicao);
        foreach ($result as $linhaBanco) {
            $elemento = $this->setDados($linhaBanco);
            $dados[] = $elemento;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado Elemento
     * com objetivo de servir as funções getTabela, getLista e getElemento
     *
     * @param array $linha
     * @return objeto Elemento
     */
    private function setDados($dados)
    {
        $elemento = new Elemento();
        $elemento->setIdElemento($dados['principal']);
        $elemento->setRotuloElemento($dados['rotulo_elemento']);
        $elemento->setIdentificadorCodigo($dados['identificador_codigo']);
        $elemento->setDescricao($dados['descricao']);
        return $elemento;
    }

    /**
     * Método que insere um objeto do tipo Elemento
     * na tabela do banco de dados
     *
     * @param Elemento Objeto data transfer
     */
    public function inserirEmTransacao(Elemento $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'public.elemento_id_elemento_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
}