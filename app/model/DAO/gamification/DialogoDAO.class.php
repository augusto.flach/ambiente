<?php

/**
 * Classe de modelo referente ao objeto Dialogo para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.gamification
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 29-07-2016(Gerado automaticamente - GC - 1.0 02/11/2015)
 */
class DialogoDAO extends AbstractDAO
{

    /**
     * Construtor da classe DialogoDAO esse metodo  
     * instancia o Modelo padrão conectando o mesmo ao banco de dados
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * A partir da consulta na view
     * 
     * <code>
        CREATE OR REPLACE VIEW gamification.dados_dialogo_completo AS 
            SELECT d.id_dialogo, d.id_idioma, m.id_missao, m.id_jogo, 
                   m.identificador_codigo as "codigoMissao", dt.tipo, e.identificador_codigo as "codigoElemento",  d.dialogo, d.ordem_missao, d.id_dialogo_particionado, d.id_idioma_particionado
                    FROM gamification.dialogo d
                    JOIN gamification.elemento e ON d.id_elemento = e.id_elemento
                    JOIN gamification.missao m ON d.id_missao = m.id_missao
                    JOIN gamification.dialogo_tipo dt ON d.id_dialogo_tipo = dt.id_dialogo_tipo
                   ORDER BY id_missao, "codigoElemento", ordem_missao;
      </code>
     * 
     * 
     * @param type $idioma
     * @param type $fase
     * @return array
     */
    public function getArvore($idioma =1, $fase= '*'){
        $result = $this->queryTable('public.dados_dialogo_completo', '*', false, 'id_missao'
        );
        $dados = array();
        $missao = 1; 
        foreach ($result as $linhaBanco) {     
            extract($linhaBanco);  
            if($missao != $codigoMissao){
                $tmpDialog = array();
                $missao = $codigoMissao;
            }
            $tmpDialog[$codigoElemento][] =  $this->makeDialog($dialogo, $tipo, $dados, $linhaBanco);
            $dados[$codigoMissao] = $tmpDialog;            
        }
        return $dados;
    }
    
    private function makeDialog($dialogo, $tipo, &$dados, $result){
        if($tipo == 'Simple'){
            return $dialogo;
        }else{
            return array($tipo => $dialogo);
        }
    }

    /**
     * Método que retorna um array com a tabela dos dados de Diálogo
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.dialogo', 'count(id_dialogo) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable('public.dialogo ' . $tabela->getcondicao(), 'id_dialogo as principal ,
                                          id_idioma,
                                          id_dialogo_tipo,
                                          id_missao,
                                          id_elemento,
                                          ordem_missao,
                                          dialogo,
                                          ordem_particionado,
                                          id_dialogo_particionado,
                                          id_idioma_particionado'
        );
        $resultado = array(
            'page' => $tabela->getPagina(),
            'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $dialogo = $this->setDados($linhaBanco);
            $row['id'] = $dialogo->getidDialogo();
            $row['cell'] = $dialogo->getArrayJson();
            $dados[] = $row;
        }
        $resultado['rows'] = $dados;
        return $resultado;
    }

    /**
     * Método que retorna um objeto do tipo Dialogo
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return Dialogo - Objeto data transfer
     */
    public function getByID($id)
    {
        $dialogo = new Dialogo();
        $consulta = $this->queryTable($dialogo->getTable(), 'id_dialogo as principal ,
                                          id_idioma,
                                          id_dialogo_tipo,
                                          id_missao,
                                          id_elemento,
                                          ordem_missao,
                                          dialogo,
                                          ordem_particionado,
                                          id_dialogo_particionado,
                                          id_idioma_particionado', 'id_dialogo =' . $id);
        if ($consulta) {
            $dialogo = $this->setDados($consulta->fetch());
            return $dialogo;
        } else {
            throw new EntradaDeDadosException();
        }
    }

    /**
     * Método que retorna um array de objetos Dialogo
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos Dialogo
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable('public.dialogo ', 'id_dialogo as principal ,
                                          id_idioma,
                                          id_dialogo_tipo,
                                          id_missao,
                                          id_elemento,
                                          ordem_missao,
                                          dialogo,
                                          ordem_particionado,
                                          id_dialogo_particionado,
                                          id_idioma_particionado', $condicao);
        foreach ($result as $linhaBanco) {
            $dialogo = $this->setDados($linhaBanco);
            $dados[] = $dialogo;
        }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado Dialogo
     * com objetivo de servir as funções getTabela, getLista e getDialogo
     *
     * @param array $linha
     * @return objeto Dialogo
     */
    private function setDados($dados)
    {
        $dialogo = new Dialogo();
        $dialogo->setIdDialogo($dados['principal']);
        $dialogo->setIdIdioma($dados['id_idioma']);
        $dialogo->setIdDialogoTipo($dados['id_dialogo_tipo']);
        $dialogo->setIdMissao($dados['id_missao']);
        $dialogo->setIdElemento($dados['id_elemento']);
        $dialogo->setOrdemMissao($dados['ordem_missao']);
        $dialogo->setDialogo($dados['dialogo']);
        $dialogo->setOrdemParticionado($dados['ordem_particionado']);
        $dialogo->setIdDialogoParticionado($dados['id_dialogo_particionado']);
        $dialogo->setIdIdiomaParticionado($dados['id_idioma_particionado']);
        return $dialogo;
    }

    /**
     * Método que insere um objeto do tipo Dialogo
     * na tabela do banco de dados
     *
     * @param Dialogo Objeto data transfer
     */
    public function inserirEmTransacao(Dialogo $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'public.dialogo_id_dialogo_seq';
            $id = $this->DB()->lastInsertId($sequencia);
            $this->DB()->commit();
            return $id;
        } else {
            return false;
        }
    }

}
