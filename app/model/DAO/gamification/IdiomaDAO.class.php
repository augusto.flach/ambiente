<?php

/**
 * Classe de modelo referente ao objeto Idioma para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.gamification
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 29-07-2016(Gerado automaticamente - GC - 1.0 02/11/2015)
 */

class IdiomaDAO extends AbstractDAO 
{

    /**
    * Construtor da classe IdiomaDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Idioma
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.idioma', 'count(id_idioma) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.idioma ' . $tabela->getcondicao(), 
                                         'id_idioma as principal ,
                                          idioma,
                                          codigo_i18n'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $idioma = $this->setDados($linhaBanco);
            $row['id'] = $idioma->getidIdioma();
            $row['cell'] = $idioma->getArrayJson();
            $dados[] = $row;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

     /**
     * Método que retorna um objeto do tipo Idioma
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return Idioma - Objeto data transfer
     */
    public function getByID($id) {
        $idioma = new Idioma();
        $consulta = $this->queryTable($idioma->getTable(),
                                         'id_idioma as principal ,
                                          idioma,
                                          codigo_i18n',
                        'id_idioma ='. $id );
        if ($consulta) {
            $idioma = $this->setDados($consulta->fetch());
            return $idioma;
        } else {
             throw new EntradaDeDadosException();
        }
     }
     /**
     * Método que retorna um array de objetos Idioma
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos Idioma
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.idioma ', 
                                         'id_idioma as principal ,
                                          idioma,
                                          codigo_i18n',
            $condicao);
        foreach ($result as $linhaBanco) {
            $idioma = $this->setDados($linhaBanco);
            $dados[] = $idioma;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado Idioma
     * com objetivo de servir as funções getTabela, getLista e getIdioma
     *
     * @param array $linha
     * @return objeto Idioma
     */
    private function setDados($dados)
    {
        $idioma = new Idioma();
        $idioma->setIdIdioma($dados['principal']);
        $idioma->setIdioma($dados['idioma']);
        $idioma->setCodigoI18n($dados['codigo_i18n']);
        return $idioma;
    }

    /**
     * Método que insere um objeto do tipo Idioma
     * na tabela do banco de dados
     *
     * @param Idioma Objeto data transfer
     */
    public function inserirEmTransacao(Idioma $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'public.idioma_id_idioma_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
}