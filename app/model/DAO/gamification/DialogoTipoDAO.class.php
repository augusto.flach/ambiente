<?php

/**
 * Classe de modelo referente ao objeto DialogoTipo para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.gamification
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 29-07-2016(Gerado automaticamente - GC - 1.0 02/11/2015)
 */

class DialogoTipoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe DialogoTipoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Diálogo tipo
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.dialogo_tipo', 'count(id_dialogo_tipo) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.dialogo_tipo ' . $tabela->getcondicao(), 
                                         'id_dialogo_tipo as principal ,
                                          tipo,
                                          descricao'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $dialogoTipo = $this->setDados($linhaBanco);
            $row['id'] = $dialogoTipo->getidDialogoTipo();
            $row['cell'] = $dialogoTipo->getArrayJson();
            $dados[] = $row;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

     /**
     * Método que retorna um objeto do tipo DialogoTipo
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return DialogoTipo - Objeto data transfer
     */
    public function getByID($id) {
        $dialogoTipo = new DialogoTipo();
        $consulta = $this->queryTable($dialogoTipo->getTable(),
                                         'id_dialogo_tipo as principal ,
                                          tipo,
                                          descricao',
                        'id_dialogo_tipo ='. $id );
        if ($consulta) {
            $dialogoTipo = $this->setDados($consulta->fetch());
            return $dialogoTipo;
        } else {
             throw new EntradaDeDadosException();
        }
     }
     /**
     * Método que retorna um array de objetos DialogoTipo
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos DialogoTipo
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.dialogo_tipo ', 
                                         'id_dialogo_tipo as principal ,
                                          tipo,
                                          descricao',
            $condicao);
        foreach ($result as $linhaBanco) {
            $dialogoTipo = $this->setDados($linhaBanco);
            $dados[] = $dialogoTipo;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado DialogoTipo
     * com objetivo de servir as funções getTabela, getLista e getDialogoTipo
     *
     * @param array $linha
     * @return objeto DialogoTipo
     */
    private function setDados($dados)
    {
        $dialogoTipo = new DialogoTipo();
        $dialogoTipo->setIdDialogoTipo($dados['principal']);
        $dialogoTipo->setTipo($dados['tipo']);
        $dialogoTipo->setDescricao($dados['descricao']);
        return $dialogoTipo;
    }

    /**
     * Método que insere um objeto do tipo DialogoTipo
     * na tabela do banco de dados
     *
     * @param DialogoTipo Objeto data transfer
     */
    public function inserirEmTransacao(DialogoTipo $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'public.dialogo_tipo_id_dialogo_tipo_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
}