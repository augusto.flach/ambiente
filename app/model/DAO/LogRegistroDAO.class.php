<?php

/**
 * Classe de modelo referente ao objeto LogRegistro para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.gamification
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 29-07-2016(Gerado automaticamente - GC - 1.0 02/11/2015)
 */

class LogRegistroDAO extends AbstractDAO 
{

    /**
    * Construtor da classe LogRegistroDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um array com a tabela dos dados de Log registro
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable('public.log_registro', 'count(id_log_registro) as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable(  'public.log_registro ' . $tabela->getcondicao(), 
                                         'id_log_registro as principal ,
                                          id_log,
                                          registro_tempo,
                                          acerto,
                                          registro_info,
                                          objeto_origem,
                                          objeto_destino,
                                          id_objeto_origem,
                                          id_objeto_destino'
                                       );
        $resultado = array(
            'page' => $tabela->getPagina(),
          'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = array();
            $logRegistro = $this->setDados($linhaBanco);
            $row['id'] = $logRegistro->getidLogRegistro();
            $row['cell'] = $logRegistro->getArrayJson();
            $dados[] = $row;
       }
        $resultado['rows'] = $dados;
        return $resultado;
    }

     /**
     * Método que retorna um objeto do tipo LogRegistro
     * sendo determinado pelo identifcador do mesmo na tabela
     *
     * @param integer $id - Identificador do dado
     * @return LogRegistro - Objeto data transfer
     */
    public function getByID($id) {
        $logRegistro = new LogRegistro();
        $consulta = $this->queryTable($logRegistro->getTable(),
                                         'id_log_registro as principal ,
                                          id_log,
                                          registro_tempo,
                                          acerto,
                                          registro_info,
                                          objeto_origem,
                                          objeto_destino,
                                          id_objeto_origem,
                                          id_objeto_destino',
                        'id_log_registro ='. $id );
        if ($consulta) {
            $logRegistro = $this->setDados($consulta->fetch());
            return $logRegistro;
        } else {
             throw new EntradaDeDadosException();
        }
     }
     /**
     * Método que retorna um array de objetos LogRegistro
     * sendo determinado pelo parâmetro $condicao
     *
     * @param String $condicao - Condição da consulta
     * @return Array de objetos LogRegistro
     */
    public function getLista($condicao = false)
    {
        $dados = array();
        $result = $this->queryTable(  'public.log_registro ', 
                                         'id_log_registro as principal ,
                                          id_log,
                                          registro_tempo,
                                          acerto,
                                          registro_info,
                                          objeto_origem,
                                          objeto_destino,
                                          id_objeto_origem,
                                          id_objeto_destino',
            $condicao);
        foreach ($result as $linhaBanco) {
            $logRegistro = $this->setDados($linhaBanco);
            $dados[] = $logRegistro;
       }
        return $dados;
    }

    /**
     * Método Private que retorna um objeto setado LogRegistro
     * com objetivo de servir as funções getTabela, getLista e getLogRegistro
     *
     * @param array $linha
     * @return objeto LogRegistro
     */
    private function setDados($dados)
    {
        $logRegistro = new LogRegistro();
        $logRegistro->setIdLogRegistro($dados['principal']);
        $logRegistro->setIdLog($dados['id_log']);
        $logRegistro->setRegistroTempo($dados['registro_tempo']);
        $logRegistro->setAcerto($dados['acerto']);
        $logRegistro->setRegistroInfo($dados['registro_info']);
        $logRegistro->setObjetoOrigem($dados['objeto_origem']);
        $logRegistro->setObjetoDestino($dados['objeto_destino']);
        $logRegistro->setIdObjetoOrigem($dados['id_objeto_origem']);
        $logRegistro->setIdObjetoDestino($dados['id_objeto_destino']);
        return $logRegistro;
    }

    /**
     * Método que insere um objeto do tipo LogRegistro
     * na tabela do banco de dados
     *
     * @param LogRegistro Objeto data transfer
     */
    public function inserirEmTransacao(LogRegistro $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = 'gamification.log_registro_id_log_registro_seq';
        $id = $this->DB()->lastInsertId($sequencia);
        $this->DB()->commit();
        return $id;
    } else {
        return false;
    }
   }
}