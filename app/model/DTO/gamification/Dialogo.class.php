<?php
/**
 * Classe para a transferencia de dados de Dialogo entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 29-07-2016(Gerado Automaticamente com GC - 1.0 02/11/2015)
 */

 class Dialogo implements DTOInterface
 {
    use core\model\DTOTrait;

    private $idDialogo;
    private $idIdioma;
    private $idDialogoTipo;
    private $idMissao;
    private $idElemento;
    private $ordemMissao = 1;
    private $dialogo;
    private $ordemParticionado = 1;
    private $idDialogoParticionado;
    private $idIdiomaParticionado;
    private $isValid;
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.dialogo')
    {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idDialogo
     *
     * @return String - Valor da variável idDialogo
     */
    public function getIdDialogo()
     {
        return $this->idDialogo;
    }

    /**
     * Método que seta o valor da variável idDialogo
     *
     * @param String $idDialogo - Valor da variável idDialogo
     */
    public function setIdDialogo($idDialogo)
    {
         $idDialogo = trim($idDialogo);
          if(empty($idDialogo)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id diálogo não pode ser nulo!';
                return false;
          }
         $this->idDialogo = $idDialogo;
         return true;
    }

    /**
     * Método que retorna o valor da variável idIdioma
     *
     * @return Inteiro - Valor da variável idIdioma
     */
    public function getIdIdioma()
     {
        return $this->idIdioma;
    }

    /**
     * Método que seta o valor da variável idIdioma
     *
     * @param Inteiro $idIdioma - Valor da variável idIdioma
     */
    public function setIdIdioma($idIdioma)
    {
         $idIdioma = trim($idIdioma);
          if(empty($idIdioma)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id idioma não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idIdioma) && is_int($idIdioma + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id idioma é um número inteiro inválido!';
                return false;
          }
          $this->idIdioma = $idIdioma;
          return true;
    }

    /**
     * Método que retorna o valor da variável idDialogoTipo
     *
     * @return Inteiro - Valor da variável idDialogoTipo
     */
    public function getIdDialogoTipo()
     {
        return $this->idDialogoTipo;
    }

    /**
     * Método que seta o valor da variável idDialogoTipo
     *
     * @param Inteiro $idDialogoTipo - Valor da variável idDialogoTipo
     */
    public function setIdDialogoTipo($idDialogoTipo)
    {
         $idDialogoTipo = trim($idDialogoTipo);
          if(empty($idDialogoTipo)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id diálogo tipo não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idDialogoTipo) && is_int($idDialogoTipo + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id diálogo tipo é um número inteiro inválido!';
                return false;
          }
          $this->idDialogoTipo = $idDialogoTipo;
          return true;
    }

    /**
     * Método que retorna o valor da variável idMissao
     *
     * @return Inteiro - Valor da variável idMissao
     */
    public function getIdMissao()
     {
        return $this->idMissao;
    }

    /**
     * Método que seta o valor da variável idMissao
     *
     * @param Inteiro $idMissao - Valor da variável idMissao
     */
    public function setIdMissao($idMissao)
    {
         $idMissao = trim($idMissao);
          if(empty($idMissao)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id missão não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idMissao) && is_int($idMissao + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id missão é um número inteiro inválido!';
                return false;
          }
          $this->idMissao = $idMissao;
          return true;
    }

    /**
     * Método que retorna o valor da variável idElemento
     *
     * @return String - Valor da variável idElemento
     */
    public function getIdElemento()
     {
        return $this->idElemento;
    }

    /**
     * Método que seta o valor da variável idElemento
     *
     * @param String $idElemento - Valor da variável idElemento
     */
    public function setIdElemento($idElemento)
    {
         $idElemento = trim($idElemento);
          if(empty($idElemento)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id elemento não pode ser nulo!';
                return false;
          }
         $this->idElemento = $idElemento;
         return true;
    }

    /**
     * Método que retorna o valor da variável ordemMissao
     *
     * @return Inteiro - Valor da variável ordemMissao
     */
    public function getOrdemMissao()
     {
        return $this->ordemMissao;
    }

    /**
     * Método que seta o valor da variável ordemMissao
     *
     * @param Inteiro $ordemMissao - Valor da variável ordemMissao
     */
    public function setOrdemMissao($ordemMissao)
    {
         $ordemMissao = trim($ordemMissao);
          if(!(is_numeric($ordemMissao) && is_int($ordemMissao + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Ordem missão é um número inteiro inválido!';
                return false;
          }
          $this->ordemMissao = $ordemMissao;
          return true;
    }

    /**
     * Método que retorna o valor da variável dialogo
     *
     * @return String - Valor da variável dialogo
     */
    public function getDialogo()
     {
        return $this->dialogo;
    }

    /**
     * Método que seta o valor da variável dialogo
     *
     * @param String $dialogo - Valor da variável dialogo
     */
    public function setDialogo($dialogo)
    {
         $dialogo = trim($dialogo);
          if(empty($dialogo)){
                $GLOBALS['ERROS'][] = 'O valor informado em Diálogo não pode ser nulo!';
                return false;
          }
         $this->dialogo = $dialogo;
         return true;
    }

    /**
     * Método que retorna o valor da variável ordemParticionado
     *
     * @return Inteiro - Valor da variável ordemParticionado
     */
    public function getOrdemParticionado()
     {
        return $this->ordemParticionado;
    }

    /**
     * Método que seta o valor da variável ordemParticionado
     *
     * @param Inteiro $ordemParticionado - Valor da variável ordemParticionado
     */
    public function setOrdemParticionado($ordemParticionado)
    {
         $ordemParticionado = trim($ordemParticionado);
          if(empty($ordemParticionado)){
                $GLOBALS['ERROS'][] = 'O valor informado em Ordem particionado não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($ordemParticionado) && is_int($ordemParticionado + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Ordem particionado é um número inteiro inválido!';
                return false;
          }
          $this->ordemParticionado = $ordemParticionado;
          return true;
    }

    /**
     * Método que retorna o valor da variável idDialogoParticionado
     *
     * @return String - Valor da variável idDialogoParticionado
     */
    public function getIdDialogoParticionado()
     {
        return $this->idDialogoParticionado;
    }

    /**
     * Método que seta o valor da variável idDialogoParticionado
     *
     * @param String $idDialogoParticionado - Valor da variável idDialogoParticionado
     */
    public function setIdDialogoParticionado($idDialogoParticionado)
    {
         $idDialogoParticionado = trim($idDialogoParticionado);
         $this->idDialogoParticionado = $idDialogoParticionado;
         return true;
    }

    /**
     * Método que retorna o valor da variável idIdiomaParticionado
     *
     * @return Inteiro - Valor da variável idIdiomaParticionado
     */
    public function getIdIdiomaParticionado()
     {
        return $this->idIdiomaParticionado;
    }

    /**
     * Método que seta o valor da variável idIdiomaParticionado
     *
     * @param Inteiro $idIdiomaParticionado - Valor da variável idIdiomaParticionado
     */
    public function setIdIdiomaParticionado($idIdiomaParticionado)
    {
         $idIdiomaParticionado = trim($idIdiomaParticionado);
          if(!(is_numeric($idIdiomaParticionado) && is_int($idIdiomaParticionado + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id idioma particionado é um número inteiro inválido!';
                return false;
          }
          $this->idIdiomaParticionado = $idIdiomaParticionado;
          return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idDialogo,
             $this->idIdioma,
             $this->idDialogoTipo,
             $this->idMissao,
             $this->idElemento,
             $this->ordemMissao,
             $this->dialogo,
             $this->ordemParticionado,
             $this->idDialogoParticionado,
             $this->idIdiomaParticionado
        );
     }


    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return ;
     }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_dialogo = ' . $this->idDialogo.' AND id_idioma = ' . $this->idIdioma;
     }
}
