<?php
/**
 * Classe para a transferencia de dados de Missao entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 29-07-2016(Gerado Automaticamente com GC - 1.0 02/11/2015)
 */

 class Missao implements DTOInterface
 {
    use core\model\DTOTrait;

    private $idMissao;
    private $idJogo;
    private $nomeMissao;
    private $descricaoMissao;
    private $linkMissao;
    private $isValid;
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.missao')
    {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idMissao
     *
     * @return Inteiro - Valor da variável idMissao
     */
    public function getIdMissao()
     {
        return $this->idMissao;
    }

    /**
     * Método que seta o valor da variável idMissao
     *
     * @param Inteiro $idMissao - Valor da variável idMissao
     */
    public function setIdMissao($idMissao)
    {
         $idMissao = trim($idMissao);
          if(empty($idMissao)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id missão não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idMissao) && is_int($idMissao + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id missão é um número inteiro inválido!';
                return false;
          }
          $this->idMissao = $idMissao;
          return true;
    }

    /**
     * Método que retorna o valor da variável idJogo
     *
     * @return Inteiro - Valor da variável idJogo
     */
    public function getIdJogo()
     {
        return $this->idJogo;
    }

    /**
     * Método que seta o valor da variável idJogo
     *
     * @param Inteiro $idJogo - Valor da variável idJogo
     */
    public function setIdJogo($idJogo)
    {
         $idJogo = trim($idJogo);
          if(empty($idJogo)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id jogo não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idJogo) && is_int($idJogo + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id jogo é um número inteiro inválido!';
                return false;
          }
          $this->idJogo = $idJogo;
          return true;
    }

    /**
     * Método que retorna o valor da variável nomeMissao
     *
     * @return String - Valor da variável nomeMissao
     */
    public function getNomeMissao()
     {
        return $this->nomeMissao;
    }

    /**
     * Método que seta o valor da variável nomeMissao
     *
     * @param String $nomeMissao - Valor da variável nomeMissao
     */
    public function setNomeMissao($nomeMissao)
    {
         $nomeMissao = trim($nomeMissao);
         $this->nomeMissao = $nomeMissao;
         return true;
    }

    /**
     * Método que retorna o valor da variável descricaoMissao
     *
     * @return String - Valor da variável descricaoMissao
     */
    public function getDescricaoMissao()
     {
        return $this->descricaoMissao;
    }

    /**
     * Método que seta o valor da variável descricaoMissao
     *
     * @param String $descricaoMissao - Valor da variável descricaoMissao
     */
    public function setDescricaoMissao($descricaoMissao)
    {
         $descricaoMissao = trim($descricaoMissao);
         $this->descricaoMissao = $descricaoMissao;
         return true;
    }

    /**
     * Método que retorna o valor da variável linkMissao
     *
     * @return String - Valor da variável linkMissao
     */
    public function getLinkMissao()
     {
        return $this->linkMissao;
    }

    /**
     * Método que seta o valor da variável linkMissao
     *
     * @param String $linkMissao - Valor da variável linkMissao
     */
    public function setLinkMissao($linkMissao)
    {
         $linkMissao = trim($linkMissao);
         $this->linkMissao = $linkMissao;
         return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idMissao,
             $this->idJogo,
             $this->nomeMissao,
             $this->descricaoMissao,
             $this->linkMissao
        );
     }


    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return $this->idMissao;
     }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_missao = ' . $this->idMissao;
     }
}
