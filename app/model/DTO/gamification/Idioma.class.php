<?php
/**
 * Classe para a transferencia de dados de Idioma entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 29-07-2016(Gerado Automaticamente com GC - 1.0 02/11/2015)
 */

 class Idioma implements DTOInterface
 {
    use core\model\DTOTrait;

    private $idIdioma;
    private $idioma;
    private $codigoI18n;
    private $isValid;
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.idioma')
    {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idIdioma
     *
     * @return Inteiro - Valor da variável idIdioma
     */
    public function getIdIdioma()
     {
        return $this->idIdioma;
    }

    /**
     * Método que seta o valor da variável idIdioma
     *
     * @param Inteiro $idIdioma - Valor da variável idIdioma
     */
    public function setIdIdioma($idIdioma)
    {
         $idIdioma = trim($idIdioma);
          if(empty($idIdioma)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id idioma não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idIdioma) && is_int($idIdioma + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id idioma é um número inteiro inválido!';
                return false;
          }
          $this->idIdioma = $idIdioma;
          return true;
    }

    /**
     * Método que retorna o valor da variável idioma
     *
     * @return String - Valor da variável idioma
     */
    public function getIdioma()
     {
        return $this->idioma;
    }

    /**
     * Método que seta o valor da variável idioma
     *
     * @param String $idioma - Valor da variável idioma
     */
    public function setIdioma($idioma)
    {
         $idioma = trim($idioma);
          if(empty($idioma)){
                $GLOBALS['ERROS'][] = 'O valor informado em Idioma não pode ser nulo!';
                return false;
          }
         $this->idioma = $idioma;
         return true;
    }

    /**
     * Método que retorna o valor da variável codigoI18n
     *
     * @return String - Valor da variável codigoI18n
     */
    public function getCodigoI18n()
     {
        return $this->codigoI18n;
    }

    /**
     * Método que seta o valor da variável codigoI18n
     *
     * @param String $codigoI18n - Valor da variável codigoI18n
     */
    public function setCodigoI18n($codigoI18n)
    {
         $codigoI18n = trim($codigoI18n);
         $this->codigoI18n = $codigoI18n;
         return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idIdioma,
             $this->idioma,
             $this->codigoI18n
        );
     }


    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return $this->idIdioma;
     }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_idioma = ' . $this->idIdioma;
     }
}
