<?php
/**
 * Classe para a transferencia de dados de DialogoTipo entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 29-07-2016(Gerado Automaticamente com GC - 1.0 02/11/2015)
 */

 class DialogoTipo implements DTOInterface
 {
    use core\model\DTOTrait;

    private $idDialogoTipo;
    private $tipo;
    private $descricao;
    private $isValid;
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.dialogo_tipo')
    {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idDialogoTipo
     *
     * @return Inteiro - Valor da variável idDialogoTipo
     */
    public function getIdDialogoTipo()
     {
        return $this->idDialogoTipo;
    }

    /**
     * Método que seta o valor da variável idDialogoTipo
     *
     * @param Inteiro $idDialogoTipo - Valor da variável idDialogoTipo
     */
    public function setIdDialogoTipo($idDialogoTipo)
    {
         $idDialogoTipo = trim($idDialogoTipo);
          if(empty($idDialogoTipo)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id diálogo tipo não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idDialogoTipo) && is_int($idDialogoTipo + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id diálogo tipo é um número inteiro inválido!';
                return false;
          }
          $this->idDialogoTipo = $idDialogoTipo;
          return true;
    }

    /**
     * Método que retorna o valor da variável tipo
     *
     * @return String - Valor da variável tipo
     */
    public function getTipo()
     {
        return $this->tipo;
    }

    /**
     * Método que seta o valor da variável tipo
     *
     * @param String $tipo - Valor da variável tipo
     */
    public function setTipo($tipo)
    {
         $tipo = trim($tipo);
          if(empty($tipo)){
                $GLOBALS['ERROS'][] = 'O valor informado em Tipo não pode ser nulo!';
                return false;
          }
         $this->tipo = $tipo;
         return true;
    }

    /**
     * Método que retorna o valor da variável descricao
     *
     * @return String - Valor da variável descricao
     */
    public function getDescricao()
     {
        return $this->descricao;
    }

    /**
     * Método que seta o valor da variável descricao
     *
     * @param String $descricao - Valor da variável descricao
     */
    public function setDescricao($descricao)
    {
         $descricao = trim($descricao);
         $this->descricao = $descricao;
         return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idDialogoTipo,
             $this->tipo,
             $this->descricao
        );
     }


    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return $this->idDialogoTipo;
     }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_dialogo_tipo = ' . $this->idDialogoTipo;
     }
}
