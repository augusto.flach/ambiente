<?php
/**
 * Classe para a transferencia de dados de Usuario entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 25-02-2016(Gerado Automaticamente com GC - 1.0 02/11/2015)
 */

 class Usuario implements DTOInterface, JsonSerializable
 {
    use core\model\DTOTrait;

    private $idUsuario;
    private $nomeCompleto;
    private $email;
    private $matricula;
    private $usuario;
    private $isValid;
    private $table;
    private $foto;
    
    
    function getFoto() {
        return $this->foto;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

        /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.usuario')
    {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idUsuario
     *
     * @return Inteiro - Valor da variável idUsuario
     */
    public function getIdUsuario()
     {
        return $this->idUsuario;
    }

    /**
     * Método que seta o valor da variável idUsuario
     *
     * @param Inteiro $idUsuario - Valor da variável idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
         $idUsuario = trim($idUsuario);
          if(empty($idUsuario)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id usuário não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idUsuario) && is_int($idUsuario + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id usuário é um número inteiro inválido!';
                return false;
          }
          $this->idUsuario = $idUsuario;
          return true;
    }

    /**
     * Método que retorna o valor da variável nomeCompleto
     *
     * @return String - Valor da variável nomeCompleto
     */
    public function getNomeCompleto()
     {
        return $this->nomeCompleto;
    }

    /**
     * Método que seta o valor da variável nomeCompleto
     *
     * @param String $nomeCompleto - Valor da variável nomeCompleto
     */
    public function setNomeCompleto($nomeCompleto)
    {
         $nomeCompleto = trim($nomeCompleto);
          if(empty($nomeCompleto)){
                $GLOBALS['ERROS'][] = 'O valor informado em Nome completo não pode ser nulo!';
                return false;
          }
         $this->nomeCompleto = $nomeCompleto;
         return true;
    }

    /**
     * Método que retorna o valor da variável email
     *
     * @return String - Valor da variável email
     */
    public function getEmail()
     {
        return $this->email;
    }

    /**
     * Método que seta o valor da variável email
     *
     * @param String $email - Valor da variável email
     */
    public function setEmail($email)
    {
         $email = trim($email);
          if(empty($email)){
                $GLOBALS['ERROS'][] = 'O valor informado em Email não pode ser nulo!';
                return false;
          }
         $this->email = $email;
         return true;
    }

    /**
     * Método que retorna o valor da variável matricula
     *
     * @return Inteiro - Valor da variável matricula
     */
    public function getMatricula()
     {
        return $this->matricula;
    }

    /**
     * Método que seta o valor da variável matricula
     *
     * @param Inteiro $matricula - Valor da variável matricula
     */
    public function setMatricula($matricula)
    {
         $matricula = trim($matricula);
          if(empty($matricula)){
                $GLOBALS['ERROS'][] = 'O valor informado em Matricula não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($matricula) && is_int($matricula + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Matricula é um número inteiro inválido!';
                return false;
          }
          $this->matricula = $matricula;
          return true;
    }

    /**
     * Método que retorna o valor da variável usuario
     *
     * @return String - Valor da variável usuario
     */
    public function getUsuario()
     {
        return $this->usuario;
    }

    /**
     * Método que seta o valor da variável usuario
     *
     * @param String $usuario - Valor da variável usuario
     */
    public function setUsuario($usuario)
    {
         $usuario = trim($usuario);
         $this->usuario = $usuario;
         return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idUsuario,
             $this->nomeCompleto,
             $this->email,
             $this->matricula,
             $this->usuario
        );
     }


    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return $this->idUsuario;
     }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_usuario = ' . $this->idUsuario;
     }
     
     public function getLevel() {
         return 1;
     }

    public function jsonSerialize() {
        return [
            'nomeCompleto' => $this->getNomeCompleto(),
            'foto' => $this->getFoto(),
            'level' => $this->getLevel()
        ];
    }

}
