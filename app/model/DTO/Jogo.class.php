<?php
/**
 * Classe para a transferencia de dados de Jogo entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 08-08-2017(Gerado Automaticamente com GC - 1.1 11/07/2017)
 */

 class Jogo implements DTOInterface
 {
    use core\model\DTOTrait;

    private $idJogo;
    private $nome;
    private $descricao;
    private $caminhoUrl;
    private $logo;
    private $isValid;
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.jogo')
    {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idJogo
     *
     * @return Inteiro - Valor da variável idJogo
     */
    public function getIdJogo()
     {
        return $this->idJogo;
    }

    /**
     * Método que seta o valor da variável idJogo
     *
     * @param Inteiro $idJogo - Valor da variável idJogo
     */
    public function setIdJogo($idJogo)
    {
         $idJogo = trim($idJogo);
          if(empty($idJogo)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id jogo não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idJogo) && is_int($idJogo + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id jogo é um número inteiro inválido!';
                return false;
          }
          $this->idJogo = $idJogo;
          return true;
    }

    /**
     * Método que retorna o valor da variável nome
     *
     * @return String - Valor da variável nome
     */
    public function getNome()
     {
        return $this->nome;
    }

    /**
     * Método que seta o valor da variável nome
     *
     * @param String $nome - Valor da variável nome
     */
    public function setNome($nome)
    {
         $nome = trim($nome);
          if(empty($nome)){
                $GLOBALS['ERROS'][] = 'O valor informado em Nome não pode ser nulo!';
                return false;
          }
         $this->nome = $nome;
         return true;
    }

    /**
     * Método que retorna o valor da variável descricao
     *
     * @return String - Valor da variável descricao
     */
    public function getDescricao()
     {
        return $this->descricao;
    }

    /**
     * Método que seta o valor da variável descricao
     *
     * @param String $descricao - Valor da variável descricao
     */
    public function setDescricao($descricao)
    {
         $descricao = trim($descricao);
         $this->descricao = $descricao;
         return true;
    }

    /**
     * Método que retorna o valor da variável caminhoUrl
     *
     * @return String - Valor da variável caminhoUrl
     */
    public function getCaminhoUrl()
     {
        return $this->caminhoUrl;
    }

    /**
     * Método que seta o valor da variável caminhoUrl
     *
     * @param String $caminhoUrl - Valor da variável caminhoUrl
     */
    public function setCaminhoUrl($caminhoUrl)
    {
         $caminhoUrl = trim($caminhoUrl);
          if(empty($caminhoUrl)){
                $GLOBALS['ERROS'][] = 'O valor informado em Caminho url não pode ser nulo!';
                return false;
          }
         $this->caminhoUrl = $caminhoUrl;
         return true;
    }

    /**
     * Método que retorna o valor da variável logo
     *
     * @return String - Valor da variável logo
     */
    public function getLogo()
     {
        return $this->logo;
    }

    /**
     * Método que seta o valor da variável logo
     *
     * @param String $logo - Valor da variável logo
     */
    public function setLogo($logo)
    {
         $logo = trim($logo);
         $this->logo = $logo;
         return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idJogo,
             $this->nome,
             $this->descricao,
             $this->caminhoUrl,
             $this->logo
        );
     }


    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return $this->idJogo;
     }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_jogo = ' . $this->idJogo;
     }
}
