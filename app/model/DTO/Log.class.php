<?php
/**
 * Classe para a transferencia de dados de Log entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 31-08-2017(Gerado Automaticamente com GC - 1.1 11/07/2017)
 */

 class Log implements DTOInterface
 {
    use core\model\DTOTrait;

    private $idLog;
    private $idMissao;
    private $idUsuario;
    private $horaInicio;
    private $horaFim;
    private $ip;
    private $userAgent;
    private $resolucaoTela;
    private $resolucaoDocumento;
    private $isValid;
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.log')
    {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idLog
     *
     * @return Inteiro - Valor da variável idLog
     */
    public function getIdLog()
     {
        return $this->idLog;
    }

    /**
     * Método que seta o valor da variável idLog
     *
     * @param Inteiro $idLog - Valor da variável idLog
     */
    public function setIdLog($idLog)
    {
         $idLog = trim($idLog);
          if(empty($idLog)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id log não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idLog) && is_int($idLog + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id log é um número inteiro inválido!';
                return false;
          }
          $this->idLog = $idLog;
          return true;
    }

    /**
     * Método que retorna o valor da variável idMissao
     *
     * @return Inteiro - Valor da variável idMissao
     */
    public function getIdMissao()
     {
        return $this->idMissao;
    }

    /**
     * Método que seta o valor da variável idMissao
     *
     * @param Inteiro $idMissao - Valor da variável idMissao
     */
    public function setIdMissao($idMissao)
    {
         $idMissao = trim($idMissao);
          if(empty($idMissao)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id missão não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idMissao) && is_int($idMissao + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id missão é um número inteiro inválido!';
                return false;
          }
          $this->idMissao = $idMissao;
          return true;
    }

    /**
     * Método que retorna o valor da variável idUsuario
     *
     * @return Inteiro - Valor da variável idUsuario
     */
    public function getIdUsuario()
     {
        return $this->idUsuario;
    }

    /**
     * Método que seta o valor da variável idUsuario
     *
     * @param Inteiro $idUsuario - Valor da variável idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
         $idUsuario = trim($idUsuario);
          if(empty($idUsuario)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id usuário não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idUsuario) && is_int($idUsuario + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id usuário é um número inteiro inválido!';
                return false;
          }
          $this->idUsuario = $idUsuario;
          return true;
    }

    /**
     * Método que retorna o valor da variável horaInicio
     *
     * @return String - Valor da variável horaInicio
     */
    public function getHoraInicio()
     {
        return $this->horaInicio;
    }

    /**
     * Método que seta o valor da variável horaInicio
     *
     * @param String $horaInicio - Valor da variável horaInicio
     */
    public function setHoraInicio($horaInicio)
    {
         $horaInicio = trim($horaInicio);
            if(is_numeric($horaInicio)){
            $horaInicio = date('Y-m-d h:i:s', round($horaInicio/1000));
        }

         $this->horaInicio = $horaInicio;
         return true;
    }

    /**
     * Método que retorna o valor da variável horaFim
     *
     * @return String - Valor da variável horaFim
     */
    public function getHoraFim()
     {
        return $this->horaFim;
    }

    /**
     * Método que seta o valor da variável horaFim
     *
     * @param String $horaFim - Valor da variável horaFim
     */
    public function setHoraFim($horaFim)
    {
         $horaFim = trim($horaFim);
         $this->horaFim = $horaFim;
         return true;
    }

    /**
     * Método que retorna o valor da variável ip
     *
     * @return String - Valor da variável ip
     */
    public function getIp()
     {
        return $this->ip;
    }

    /**
     * Método que seta o valor da variável ip
     *
     * @param String $ip - Valor da variável ip
     */
    public function setIp($ip)
    {
         $ip = trim($ip);
         $this->ip = $ip;
         return true;
    }

    /**
     * Método que retorna o valor da variável userAgent
     *
     * @return String - Valor da variável userAgent
     */
    public function getUserAgent()
     {
        return $this->userAgent;
    }

    /**
     * Método que seta o valor da variável userAgent
     *
     * @param String $userAgent - Valor da variável userAgent
     */
    public function setUserAgent($userAgent)
    {
         $userAgent = trim($userAgent);
         $this->userAgent = $userAgent;
         return true;
    }

    /**
     * Método que retorna o valor da variável resolucaoTela
     *
     * @return String - Valor da variável resolucaoTela
     */
    public function getResolucaoTela()
     {
        return $this->resolucaoTela;
    }

    /**
     * Método que seta o valor da variável resolucaoTela
     *
     * @param String $resolucaoTela - Valor da variável resolucaoTela
     */
    public function setResolucaoTela($resolucaoTela)
    {
         $resolucaoTela = trim($resolucaoTela);
         $this->resolucaoTela = $resolucaoTela;
         return true;
    }

    /**
     * Método que retorna o valor da variável resolucaoDocumento
     *
     * @return String - Valor da variável resolucaoDocumento
     */
    public function getResolucaoDocumento()
     {
        return $this->resolucaoDocumento;
    }

    /**
     * Método que seta o valor da variável resolucaoDocumento
     *
     * @param String $resolucaoDocumento - Valor da variável resolucaoDocumento
     */
    public function setResolucaoDocumento($resolucaoDocumento)
    {
         $resolucaoDocumento = trim($resolucaoDocumento);
         $this->resolucaoDocumento = $resolucaoDocumento;
         return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idLog,
             $this->idMissao,
             $this->idUsuario,
             $this->horaInicio,
             $this->horaFim,
             $this->ip,
             $this->userAgent,
             $this->resolucaoTela,
             $this->resolucaoDocumento
        );
     }


    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return $this->idLog;
     }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_log = ' . $this->idLog;
     }
}
