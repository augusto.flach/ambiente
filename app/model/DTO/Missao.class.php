<?php
/**
 * Classe para a transferencia de dados de Missao entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 10-08-2017(Gerado Automaticamente com GC - 1.1 11/07/2017)
 */

 class Missao implements DTOInterface
 {
    use core\model\DTOTrait;

    private $idMissao;
    private $idJogo;
    private $nome;
    private $descricao;
    private $isValid;
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.missao')
    {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idMissao
     *
     * @return Inteiro - Valor da variável idMissao
     */
    public function getIdMissao()
     {
        return $this->idMissao;
    }

    /**
     * Método que seta o valor da variável idMissao
     *
     * @param Inteiro $idMissao - Valor da variável idMissao
     */
    public function setIdMissao($idMissao)
    {
         $idMissao = trim($idMissao);
          if(empty($idMissao)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id missão não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idMissao) && is_int($idMissao + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id missão é um número inteiro inválido!';
                return false;
          }
          $this->idMissao = $idMissao;
          return true;
    }

    /**
     * Método que retorna o valor da variável idJogo
     *
     * @return Inteiro - Valor da variável idJogo
     */
    public function getIdJogo()
     {
        return $this->idJogo;
    }

    /**
     * Método que seta o valor da variável idJogo
     *
     * @param Inteiro $idJogo - Valor da variável idJogo
     */
    public function setIdJogo($idJogo)
    {
         $idJogo = trim($idJogo);
          if(empty($idJogo)){
                $GLOBALS['ERROS'][] = 'O valor informado em Id jogo não pode ser nulo!';
                return false;
          }
          if(!(is_numeric($idJogo) && is_int($idJogo + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id jogo é um número inteiro inválido!';
                return false;
          }
          $this->idJogo = $idJogo;
          return true;
    }

    /**
     * Método que retorna o valor da variável nome
     *
     * @return String - Valor da variável nome
     */
    public function getNome()
     {
        return $this->nome;
    }

    /**
     * Método que seta o valor da variável nome
     *
     * @param String $nome - Valor da variável nome
     */
    public function setNome($nome)
    {
         $nome = trim($nome);
         $this->nome = $nome;
         return true;
    }

    /**
     * Método que retorna o valor da variável descricao
     *
     * @return String - Valor da variável descricao
     */
    public function getDescricao()
     {
        return $this->descricao;
    }

    /**
     * Método que seta o valor da variável descricao
     *
     * @param String $descricao - Valor da variável descricao
     */
    public function setDescricao($descricao)
    {
         $descricao = trim($descricao);
         $this->descricao = $descricao;
         return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idMissao,
             $this->idJogo,
             $this->nome,
             $this->descricao
        );
     }


    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return $this->idMissao;
     }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_missao = ' . $this->idMissao;
     }
}
