<?php

/**
 * Classe para a transferencia de dados de LogRegistro entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 31-08-2017(Gerado Automaticamente com GC - 1.1 11/07/2017)
 */
class LogRegistro implements DTOInterface {

    use core\model\DTOTrait;

    private $idLogRegistro;
    private $idLog;
    private $idObjetoOrigem;
    private $idObjetoDestino;
    private $registroTempo;
    private $acerto;
    private $registroInfo;
    private $objetoOrigem;
    private $objetoDestino;
    private $isValid;
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.log_registro') {
        $this->table = $table;
    }

    /**
     * Método que retorna o valor da variável idLogRegistro
     *
     * @return String - Valor da variável idLogRegistro
     */
    public function getIdLogRegistro() {
        return $this->idLogRegistro;
    }

    /**
     * Método que seta o valor da variável idLogRegistro
     *
     * @param String $idLogRegistro - Valor da variável idLogRegistro
     */
    public function setIdLogRegistro($idLogRegistro) {
        $idLogRegistro = trim($idLogRegistro);
        if (empty($idLogRegistro)) {
            $GLOBALS['ERROS'][] = 'O valor informado em Id log registro não pode ser nulo!';
            return false;
        }
        $this->idLogRegistro = $idLogRegistro;
        return true;
    }

    /**
     * Método que retorna o valor da variável idLog
     *
     * @return Inteiro - Valor da variável idLog
     */
    public function getIdLog() {
        return $this->idLog;
    }

    /**
     * Método que seta o valor da variável idLog
     *
     * @param Inteiro $idLog - Valor da variável idLog
     */
    public function setIdLog($idLog) {
        $idLog = trim($idLog);
        if (empty($idLog)) {
            $GLOBALS['ERROS'][] = 'O valor informado em Id log não pode ser nulo!';
            return false;
        }
        if (!(is_numeric($idLog) && is_int($idLog + 0))) {
            $GLOBALS['ERROS'][] = 'O valor informado em Id log é um número inteiro inválido!';
            return false;
        }
        $this->idLog = $idLog;
        return true;
    }

    /**
     * Método que retorna o valor da variável idObjetoOrigem
     *
     * @return Inteiro - Valor da variável idObjetoOrigem
     */
    public function getIdObjetoOrigem() {
        return $this->idObjetoOrigem;
    }

    /**
     * Método que seta o valor da variável idObjetoOrigem
     *
     * @param Inteiro $idObjetoOrigem - Valor da variável idObjetoOrigem
     */
    public function setIdObjetoOrigem($idObjetoOrigem) {
        $idObjetoOrigem = trim($idObjetoOrigem);
        if (!(is_numeric($idObjetoOrigem) && is_int($idObjetoOrigem + 0))) {
            $GLOBALS['ERROS'][] = 'O valor informado em Id objeto origem é um número inteiro inválido!';
            return false;
        }
        $this->idObjetoOrigem = $idObjetoOrigem;
        return true;
    }

    /**
     * Método que retorna o valor da variável idObjetoDestino
     *
     * @return Inteiro - Valor da variável idObjetoDestino
     */
    public function getIdObjetoDestino() {
        return $this->idObjetoDestino;
    }

    /**
     * Método que seta o valor da variável idObjetoDestino
     *
     * @param Inteiro $idObjetoDestino - Valor da variável idObjetoDestino
     */
    public function setIdObjetoDestino($idObjetoDestino) {
        $idObjetoDestino = trim($idObjetoDestino);
        if (!(is_numeric($idObjetoDestino) && is_int($idObjetoDestino + 0))) {
            $GLOBALS['ERROS'][] = 'O valor informado em Id objeto destino é um número inteiro inválido!';
            return false;
        }
        $this->idObjetoDestino = $idObjetoDestino;
        return true;
    }

    /**
     * Método que retorna o valor da variável registroTempo
     *
     * @return String - Valor da variável registroTempo
     */
    public function getRegistroTempo() {
        return $this->registroTempo;
    }

    /**
     * Método que seta o valor da variável registroTempo
     *
     * @param String $registroTempo - Valor da variável registroTempo
     */
    public function setRegistroTempo($registroTempo) {
        $registroTempo = trim($registroTempo);
        if (is_numeric($registroTempo)) {
            $registroTempo = date('Y-m-d h:i:s', round($registroTempo / 1000));
        }

        $this->registroTempo = $registroTempo;
        return true;
    }

    /**
     * Método que retorna o valor da variável acerto
     *
     * @return Boolean - Valor da variável acerto
     */
    public function getAcerto() {
        return $this->acerto;
    }

    /**
     * Método que seta o valor da variável acerto
     *
     * @param Boolean $acerto - Valor da variável acerto
     */
    public function setAcerto($acerto) {
        $acerto = trim($acerto);
        $campo = trim($acerto);
        ds($acerto);
        if (empty($acerto)) {
            $this->acerto = 0;
            return true;
        }
        if ($acerto == 'f' || $acerto == 'false') {
            $this->acerto = 0;
            return true;
        }
        $this->acerto = 1;
        return true;
    }

    /**
     * Método que retorna o valor da variável registroInfo
     *
     * @return String - Valor da variável registroInfo
     */
    public function getRegistroInfo() {
        return $this->registroInfo;
    }

    /**
     * Método que seta o valor da variável registroInfo
     *
     * @param String $registroInfo - Valor da variável registroInfo
     */
    public function setRegistroInfo($registroInfo) {
        $registroInfo = trim($registroInfo);
        $this->registroInfo = $registroInfo;
        return true;
    }

    /**
     * Método que retorna o valor da variável objetoOrigem
     *
     * @return String - Valor da variável objetoOrigem
     */
    public function getObjetoOrigem() {
        return $this->objetoOrigem;
    }

    /**
     * Método que seta o valor da variável objetoOrigem
     *
     * @param String $objetoOrigem - Valor da variável objetoOrigem
     */
    public function setObjetoOrigem($objetoOrigem) {
        $objetoOrigem = trim($objetoOrigem);
        $this->objetoOrigem = $objetoOrigem;
        return true;
    }

    /**
     * Método que retorna o valor da variável objetoDestino
     *
     * @return String - Valor da variável objetoDestino
     */
    public function getObjetoDestino() {
        return $this->objetoDestino;
    }

    /**
     * Método que seta o valor da variável objetoDestino
     *
     * @param String $objetoDestino - Valor da variável objetoDestino
     */
    public function setObjetoDestino($objetoDestino) {
        $objetoDestino = trim($objetoDestino);
        $this->objetoDestino = $objetoDestino;
        return true;
    }

    /**
     * Método que retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
    public function getTable() {
        return $this->table;
    }

    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
    public function getArrayJSON() {
        return array(
            $this->idLogRegistro,
            $this->idLog,
            $this->idObjetoOrigem,
            $this->idObjetoDestino,
            $this->registroTempo,
            $this->acerto,
            $this->registroInfo,
            $this->objetoOrigem,
            $this->objetoDestino
        );
    }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getID() {
        return $this->idLogRegistro;
    }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition() {
        return 'id_log_registro = ' . $this->idLogRegistro;
    }

}
