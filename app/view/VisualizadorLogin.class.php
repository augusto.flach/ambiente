<?php

/**
 * Description of VisualizadorGeral
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
class VisualizadorLogin extends AbstractView
{

    private $mostrarNavegacao = false;

    public function __construct()
    {
        parent::__construct();
        $this->addCSS('layout');
        $this->CDN()->add('jquery');
        $this->CDN()->add('bootstrap');
    }

}
