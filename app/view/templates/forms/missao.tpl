{*Gerado automaticamente com GC - 1.1 11/07/2017*}
<fieldset class="formPadrao">
     <legend>Missão</legend>
             <input type="hidden" id="idMissao" name="idMissao" value="{$missao->getIdMissao()}" class=" form-control" required  />
         <div class="form-group">
              <label class="control-label col-sm-2" for="idJogo">Id jogo</label>
              <div class="col-sm-8">
                 <select id="idJogo" name="idJogo" class="form-control">
    		{html_options options=$listaJogo selected=$missao->getIdJogo()}
             </select>
<a href="/admin/Jogo/criarNovo/Missao" class="addData">[+] Jogo</a>

              </div>
         </div>
         <div class="form-group">
              <label class="control-label col-sm-2" for="nomeMissao">Nome missão</label>
              <div class="col-sm-8">
                 <input type="text" id="nomeMissao" name="nomeMissao" value="{$missao->getNomeMissao()}" class=" form-control"  />
              </div>
         </div>
         <div class="form-group">
              <label class="control-label col-sm-2" for="descricaoMissao">Descrição missão</label>
              <div class="col-sm-8">
                 <textarea id="descricaoMissao" name="descricaoMissao" class=" form-control" >{$missao->getDescricaoMissao()}</textarea>
              </div>
         </div>
</fieldset>

