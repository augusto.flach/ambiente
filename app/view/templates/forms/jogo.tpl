{*Gerado automaticamente com GC - 1.1 11/07/2017*}
<fieldset class="formPadrao">
     <legend>Jogo</legend>
             <input type="hidden" id="idJogo" name="idJogo" value="{$jogo->getIdJogo()}" class=" form-control" required  />
         <div class="form-group">
              <label class="control-label col-sm-2" for="nome">Nome</label>
              <div class="col-sm-8">
                 <input type="text" id="nome" name="nome" value="{$jogo->getNome()}" class=" form-control" required  />
              </div>
         </div>
         <div class="form-group">
              <label class="control-label col-sm-2" for="descricao">Descrição</label>
              <div class="col-sm-8">
                 <textarea id="descricao" name="descricao" class=" form-control" >{$jogo->getDescricao()}</textarea>
              </div>
         </div>
         <div class="form-group">
              <label class="control-label col-sm-2" for="caminhoUrl">Caminho url</label>
              <div class="col-sm-8">
                 <input type="text" id="caminhoUrl" name="caminhoUrl" value="{$jogo->getCaminhoUrl()}" class=" form-control" required  />
              </div>
         </div>
         <div class="form-group">
              <label class="control-label col-sm-2" for="logo">Logo</label>
              <div class="col-sm-8">
                 <input type="file" id="logo" name="logo" value="{$jogo->getLogo()}" class=" form-control"  />
              </div>
         </div>
</fieldset>

