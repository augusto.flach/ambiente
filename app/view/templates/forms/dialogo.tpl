{*Gerado automaticamente com GC - 1.1 11/07/2017*}
<fieldset class="formPadrao">
    <legend>Diálogo</legend>
        <input type="hidden" id="idDialogo" name="idDialogo" value="{$dialogo->getIdDialogo()}" class=" form-control" required  />

        <div class="form-group">
            <label class="control-label col-sm-2" for="idMissao">Missão</label>
            <div class="col-sm-8">
                <select id="idMissao" name="idMissao" class="form-control">
                    {html_options options=$listaMissao selected=$dialogo->getIdMissao()}
                </select>
                <a href="/admin/Missao/criarNovo/Dialogo" class="addData">[+] Missão</a>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="idDialogoParticionado">Diálogo Anterior</label>
            <div class="col-sm-8">
                <select id="idDialogoParticionado" name="idDialogoParticionado" class="form-control">
                    {html_options options=$listaDialogoParticionado selected=$dialogo->getIdDialogoParticionado()}
                </select>                
            </div>
         </div>
    
         <div class="form-group">
              <label class="control-label col-sm-2" for="dialogo">Diálogo</label>
              <div class="col-sm-8">
                  <textarea id="dialogo" name="dialogo"  class=" form-control" required>{$dialogo->getDialogo()}</textarea>
              </div>
         </div>
         <div class="form-group">
              <label class="control-label col-sm-2" for="ordemMissao">Ordem missão</label>
              <div class="col-sm-8">
                 <input type="text" id="ordemMissao" name="ordemMissao" value="{$dialogo->getOrdemMissao()}" class="validaInteiro form-control"  />
              </div>
         </div>
         <div class="form-group">
            <label class="control-label col-sm-2" for="ordemParticionado">Ordem particionado</label>
            <div class="col-sm-8">
                <input type="text" id="ordemParticionado" name="ordemParticionado" value="{$dialogo->getOrdemParticionado()}" class="validaInteiro form-control" required  />
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-sm-2" for="idTipo">Id tipo</label>
            <div class="col-sm-8">
                <select id="idTipo" name="idTipo" class="form-control">
                    {html_options options=$listaTipo selected=$dialogo->getIdTipo()}
                </select>
                <a href="/admin/Tipo/criarNovo/Dialogo" class="addData">[+] Tipo</a>
            </div>
         </div>
</fieldset>

