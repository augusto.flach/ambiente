<div class="loginPane">
    <h1 class="logoLogin center-block"><a href="/">IFRS Campus canoas</a></h1>

    <div class="container" style="margin-top:40px">

        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                {include file=$MSG_FILE}

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong> Entre com o Login e senha</strong>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="/login/logar" method="POST">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-user"></i>
                                                </span>
                                                <input class="form-control" placeholder="Usuário" name="usuario" type="text" required="required" autofocus>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-lock"></i>
                                                </span>
                                                <input class="form-control" placeholder="Senha" name="senha" type="password" required="required" value="">
                                            </div>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="remember-me"> Lembrar de mim
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-lg btn-primary btn-block" value="logar">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div class="panel-footer ">
                        <p class="center-block"> <a href="https://wifi.canoas.ifrs.edu.br/">Esqueci a senha</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
