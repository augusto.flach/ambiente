<script type="text/javascript">

	//pega o timestamp da data que ocorrera a oficina
	var dataOficina = new Date("04/18/2018 00:00:00");
	dataOficina = dataOficina.getTime();

	//pega o timestamp atual
	var dataAtual = new Date();
	dataAtual = dataAtual.getTime();

	//redireciona para as inscrições caso a data seja anterior a data da oficina
	if(dataAtual < dataOficina)
		location.href = "./games/inscricao.html";

</script>

<a href="./login/logout" id="sair">Sair</a>

<h1>Gamificação</h1>

<ul id="menu">

	<li>
		<a href="#logic-me">
			<div class="logo logic-me disable"></div>
			<h2>LOGIC.ME</h2>
		</a>
	</li>

	<li>
		<!--<a href="./games/tri-logic/index.html">-->
		<a href="#tri-logic">
			<div class="logo tri-logic disable"></div>
			<h2>TRI LOGIC</h2>
		</a>
	</li>

	<li>
		<a href="./games/oficinas.html">
			<div class="logo oficinas"></div>
			<h2>OFICINAS</h2>
		</a>	
	</li>


</ul>
