<?php

/**
 * Description of VisualizadorGeral
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
class VisualizadorAdmin extends AbstractView
{

    private $mostrarNavegacao = true;

    public function __construct()
    {
        parent::__construct();
                $this->geraMenu();

        $this->CDN()->add('jquery');
        $this->CDN()->add('bootstrap');
        $this->attValue('navegacaoMenu', $this->mostrarNavegacao);
    }

    public function naoMostrarNavegacao()
    {
        $this->attValue('navegacaoMenu', false);
    }
    
    private function geraMenu(){
        $this->addTemplate(TEMPLATES_CORE .'menu');
        $this->addTemplate('menu');
        $menu = $this->parseMenu();
        $this->attValue('_menu', $menu);
    }
    
    private function getUsuario(){
        if(isset($_SESSION['user'])){
            return unserialize($_SESSION['user']);
        }else{
            header('location:/login');
        }
    }
    
    private function parseMenu(){
        $menuFile = file_get_contents(ROOT . 'view/menus/menu_admin.json');
        
        $menuFile = str_replace('{$USUARIO}', $this->criarImagemMenu(), $menuFile);
        $menu = new Menu();
        $menu->readerJson($menuFile);
      
        return $menu;
    }
    
    private function criarImagemMenu(){
        $usuario = $this->getUsuario();
        
        $html = '<img src=\"'. $usuario->getExtra('foto') . '\" class=\"imagemMenu\" '
                . 'alt=\"Foto de ' . $usuario->getExtra('nome') . '\">';//. $usuario->getE
        return $html;
        
    }
    
  

}

