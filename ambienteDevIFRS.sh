#!/bin/bash
#
# Versão 1.0.0: Deve estar no diretorio bin para ser global
#
# Marcio Bigolin, Setembro de 2017
#
sudo service apache2 stop
clear
if [ -f ".ready" ]
then
	echo "Pronto para iniciar o ambiente..."
else
	echo "Parece que é nossa primeira execução vamos realizar algumas tarefas."
	echo "--------"
	echo "Se acontecer algum erro por aqui verifique se você tem o git devidamente instalado em seu computador."
	echo '{   "dns": ["200.17.91.4", "200.17.88.3"]}' >> daemon.json
	sudo mv daemon.json /etc/docker/daemon.json
	service docker restart
	git submodule init
	git submodule update	
	
	echo "Agora que você tem tudo pronto vou passar a bola para o Enyalius. "
        if which eny >/dev/null; then
            echo 'O enyalius está instalado na versão'
            eny --version
            echo 'Essa versão deve ser superior ou igual a' 
            grep '^# Versão ' "$0" | tail -1 | cut -d : -f 1 | tr -d \#
        else
            #TODO Ver no win de copiar isso para uma pasta do docker 
            sudo cp core/enyalius /bin/eny
            sudo chmod +x /bin/eny
        fi
	sudo eny -i
	echo "Pronto para iniciar o ambiente."
	cd public_html/games/escapeIFRS
	sudo eny yarn install
	cd ../../../
	touch .ready
fi
sudo docker run -e "WEBAPP_ROOT=public_html" -v $PWD:/app -e WEBAPP_USER_ID=$(id -u) -p 80:80 enyalius/dev:latest