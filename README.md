[![build status](https://gitlab.com/gamificacao/ambiente/badges/master/build.svg)](https://gitlab.com/gamificacao/ambiente/commits/master)

<h1>Ambiente Gamificação</h1>

Mineração de dados e gerência dos Jogos do projeto Gamificação. Nesse projeto fica os serviços SERVER-SIDE do projeto.

* [Executando todo o projeto em um computador](doc/IniciarAmbiente.md)
* [Fluxo de trabalho](doc/fluxo.md)
* [Dicas para Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [Navegação no Gitlab](https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/)


<h2>Regras de Commit</h2>

Se possível (e quando necessário), o commit deve manter um corpo estrutural padronizado.
Utilizaremos o padrão resumido do karma (http://karma-runner.github.io/1.0/dev/git-commit-msg.html)

<code> TYPE(TARGET): DESCRIPTION. </code>

O <code>TARGET</code> é o alvo de modificações do commit. Pode ser um arquivo, ou um módulo da aplicação.

<h3>Types Permitidos</h3>

* FEAT: Commits com adição de funcionalidade (Seja ela pronta ou em progresso);
* FIX: Um bugfix. Se houver uma Issue relacionada, deve-se fechá-la;
* REFACT: Algumas melhorias de código (Sejam elas de performance, de limpeza, ou de documentação);

<h3>Fechando Issues</h3>

Para fechar uma Issue cadastrada aqui no Gitlab, o commit message deve conter (Após o ponto, por padrão):

<code>COMMIT_MESSAGE. closes #ISSUE_NUMBER.</code>

<code>COMMIT_MESSAGE</code> sendo a mensagem em si e <code>ISSUE_NUMBER</code> o número da Issue aqui no Gitlab.

<h3>Mudando o status de tarefas</h3>

Se queres mudar o status de alguma tarefa ou issue, vá até ela e adicione a label.

<h3>Status permitidos</h3>

* doing: Realizando a tarefa, lembre-se de atribuir para si a tarefa, caso ela já não tenha sido passada por você;
* :cursed: É amaldiçoado mesmo. Nesse caso, a tarefa está presa em algumas linhas de código :astonished:;
* needs:review: Adicione essa label para quando quiser indicar que o trabalho a ser commitado não se encaixa com o que foi requisitado. Precisando de revisão;
* needs:test: Essa label serve para incar que o trabalho commitado necessita de testes para ser homologado;

<h3>Chamando alguem (ou @all todo mundo)</h3>

Se queres chamar alguém na sua commit message, não tenha vergonha :kissing:

<code>Basta chamar com @USER_NAME.</code>

# Lista de users

### Professores
* @marcio.bigolin - Marcio Bigolin
* @sandro.silva - Sandro Silva
* @cbalestro - Carla Odete Balestro Silva
* @leofilipe - Leonardo Filipe Batista Silva de Carvalho 

### Bolsistas
* @AufgfuA - Augusto
* @brendaab.2010 - Brenda
* @muchsousa - Bruno
* @Lariss3012 - Larissa


