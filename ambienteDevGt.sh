#!/bin/bash
#
# Versão 1.0.0: Deve estar no diretorio bin para ser global
#
# Marcio Bigolin, Setembro de 2017
#
clear
if [ -f ".ready" ]
then
	echo "Pronto para iniciar o ambiente..."
else
	echo "Parece que é nossa primeira execução vamos realizar algumas tarefas."
	echo "--------"
	echo "Se acontecer algum erro por aqui verifique se você tem o git devidamente instalado em seu computador."
	git submodule init
	git submodule update
	eny composer install
	echo "Pronto para iniciar o ambiente."
	cd public_html/games/escapeIFRS
	eny yarn install
	cd ../../../
	touch .ready
fi
docker run -e "WEBAPP_ROOT=public_html" -v $PWD:/app -e WEBAPP_USER_ID=$(id -u) -p 8088:80 enyalius/dev:latest