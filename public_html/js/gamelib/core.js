/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function salvar(id, tempoInicial, contadorDeErros){
//    $.post( "/logger/register", 
//        {   idUsuario: idUser, 
//            idMissao:fase,
//            horaInicio: tempoInicial,
//            horaFim: new Date().getTime(),
//            contadorErros: contadorDeErros,
//            tamanhoDocumento: [$(document).width(), $(document).height()],
//            tamanhoTela: [window.screen.availWidth, window.screen.availHeight]
//        } );

    $.post( "/logger/finalizaLog", 
        {   idLog: id,
            horaFim:  new Date().getTime(),
        } );
}

/**
 * 
 * @param {type} id da missão
 * @returns {data|Number}
 */
function saveInit(id){
    jQuery.ajaxSetup({async:false});
    var v = -1;
    $.post( "/logger/iniciaLog", 
        {   idMissao: id,
            horaInicio:  new Date().getTime(),
            resolucaoDocumento: $(document).width() +'x' + $(document).height(),
            resolucaoTela: window.screen.availWidth +'x' + window.screen.availHeight
        } ).done(function(data){
            v = data;
        });
     jQuery.ajaxSetup({async:true});   
     return v;
}

//function saveEventLevel(id, eventTime, targetId, receiverId, isError){
//        
//        $.post( "/logger/gravaLog", 
//        {   
//            idLog: id,
//            registroTempo: eventTime,
//            objectReceiveError: receiverId,
//            error: isError,
//            objectId: targetId
//        } )
//} 

function log(data){
    console.log(data);
        $.post( "/logger/gravaLog", 
        {   registroTempo: new Date().getTime(),
            objetoDestino:data.objectTarget,
            acerto: data.success,
            objetoOrigem: data.objectOrigin
        } ); 
    
}

function getIDUser() {
    var v = false;
    jQuery.ajaxSetup({async:false});
    $.get('/gameService/requireIdUser', function (data) {
         v = data;
    });
    jQuery.ajaxSetup({async:true});
    return v;
}

var idUser = getIDUser();