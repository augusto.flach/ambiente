<!DOCTYPE HTML>
<html>
<head>
    <title>Game teste services</title>
    <meta name="viewport" content='initial-scale=1, maximum-scale=1, user-scalable=no'>
    <meta charset="utf-8">
</head>

<body>
    <ul>
        <li><a href="#capturarId">Capturar ID</a></li>
        <li><a href="#capturarJogador">Capturar Jogador</a></li>
        <li><a href="#terminarLog">Terminar Log</a></li>
        <li><a href="#dialogo">Get Dialogo</a></li>
    </ul>
    <div id="controlLog">
        Missão: <input id="missao" />
        Log: <input id="log" />
        Elemento origem: <input id="elementoOrigem" value="alfa" />
        Elemento destino: <input id="elementoDestino" value="omega"/>
        Acerto: <input id="acerto" type="checkbox"/>
        <a href="#initLog">Init Log</a>
        <a href="#logar">Log</a>
        <a href="#fimLog">Fim Log</a>

    </div>
    <div id="result">
        <pre id="resultContent">
            
        </pre>
    </div>
    
    
    <script src="/js/dist/jquery/jquery.js"></script>
    <script src="/js/debug/enyalius/main.js"></script>
    <script src="/js/gamelib/core.js"></script>

    <script>
        var IDLog = false;
        var IDMissao = false;
        function replaceResult(url){
            $.get(url, function(data){
                $("#resultContent").html(data);
            });
        }
        
        var factory = new Object();
        factory.capturarJogador = function(){
            replaceResult('/gameService/requireCompleteUser')
        };
        
        factory.capturarId = function(){
            IDUser = getIDUser();
            $("#resultContent").html(IDUser);
        };
      
        factory.logar = function(){
          var sucesso = $('#acerto').is(':checked')? true : false;
          log({ objectOrigin: $("#elementoOrigem").val(),
                objectTarget:  $("#elementoDestino").val(),
                sucess: sucesso
                });

        };
        factory.initLog = function(){
            var id = getIdMissao();
            var idLog = saveInit(id);
            $("#log").val(idLog);
        }
        
        function getIdMissao(){
            var id = parseInt($("#missao").val());
            if(isNumber(id)){
                IDMissao = id;              
            }else{
                IDMissao = 1;
            }
            return IDMissao;            
        }
        
        //Listener 
        $('a').click(function(){
          var acao = $(this).attr('href').replace('#', '');
          factory[acao]();
        });
        
    </script>
</body>

</html>